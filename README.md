# Szoftvertervezés és -fejlesztés I.
--------------

A Szoftvertervezés és -fejlesztés I. laborokon ismertetett anyagok kódjai és példái.

## Letöltés

* .zip letöltés: Downloads >> Download repository
* cli: `git clone https://zsalab-oe@bitbucket.org/zsalab/sztf1-2020-21-1.git`

## Hasznos linkek

* Az anyagok megtalálhatók a [http://users.nik.uni-obuda.hu/zsalab/](http://users.nik.uni-obuda.hu/zsalab/) oldalamon is.
* Tavalyi laboranyagok: [https://bitbucket.org/zsalab-oe/sztf1-2019-20-1/](https://bitbucket.org/zsalab-oe/sztf1-2019-20-1/)

## Kapcsolat

Kérdés, óhaj, sóhaj, panasz esetén bátran keressetek a `gaspar.balazs {at} nik.uni-obuda.hu` e-mail címen!

-----------------
*Gáspár Balázs*