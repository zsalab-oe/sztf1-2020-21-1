﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Futoedzes
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu("futonaplo.txt");
        }

        static void Menu(string futonaplo)
        {
            int valasztott;
            do
            {
                Console.Clear();
                MenuKiir();
                valasztott = int.Parse(Console.ReadLine());
                if (valasztott == 1)
                {
                    Felvetel(futonaplo);
                }
                else if (valasztott == 2)
                {
                    Listazas(futonaplo);
                }
                else if (valasztott == 3)
                {
                    Statisztikak(futonaplo);
                }
                Console.WriteLine("A továbblépéshez nyomj entert!");
                Console.ReadLine();
            } while (valasztott != 4);
        }

        static void MenuKiir()
        {
            string menu = "";
            menu += "1: Új edzés felvitele\n";
            menu += "2: Edzések listázása\n";
            menu += "3: Statisztikák\n";
            menu += "4: Kilépés\n";
            Console.WriteLine(menu);
        }

        static void Felvetel(EdzesKezelo kezelo)
        {
            Edzes edzes = new Edzes();
            Console.Write("Dátum: ");
            edzes.Datum = DateTime.Parse(Console.ReadLine());
            Console.Write("Táv: ");
            edzes.Tav = double.Parse(Console.ReadLine());
            Console.Write("Idő: ");
            edzes.Ido = TimeSpan.Parse(Console.ReadLine());

            kezelo.Felvetel(edzes);
        }

        static void Felvetel(string futonaplo)
        {
            Console.Write("Dátum: ");
            DateTime datum = DateTime.ParseExact(Console.ReadLine(), "yyyymmdd", null);
            Console.Write("Táv: ");
            double tav = double.Parse(Console.ReadLine());
            Console.Write("Idő: ");
            TimeSpan ido = TimeSpan.Parse(Console.ReadLine());
            string adatok = $"{datum};{tav};{ido}";
            File.AppendAllText(futonaplo, adatok + "\r\n");
        }

        static void Listazas(EdzesKezelo kezelo)
        {
            Edzes[] edzesek = kezelo.Listaz();
            for (int i = 0; i < edzesek.Length; i++)
            {
                Console.WriteLine($"{edzesek[i].Adatok().Replace(';', '\t')}");
            }
        }

        static void Listazas(string futonaplo)
        {
            StreamReader sr = new StreamReader(futonaplo);
            while (!sr.EndOfStream)
            {
                Console.WriteLine(sr.ReadLine().Replace(';', '\t'));
            }
            sr.Close();
        }

        static void Statisztikak(EdzesKezelo kezelo)
        {
            string stats = kezelo.Statisztikak();
            Console.WriteLine(stats);
        }

        static void Statisztikak(string futonaplo)
        {
            int db = 0;
            double osszTav = 0;
            TimeSpan osszIdo = new TimeSpan();

            StreamReader sr = new StreamReader(futonaplo);
            while (!sr.EndOfStream)
            {
                Edzes edzes = new Edzes(sr.ReadLine());
                osszTav += edzes.Tav;
                osszIdo += edzes.Ido;
                db++;
            }
            sr.Close();
            TimeSpan atlagIdo = TimeSpan.FromSeconds(osszIdo.TotalSeconds / db);

            string stats = "";
            stats += $"Edzések száma: {db}\r\n";
            stats += $"Össztáv: {osszTav}\r\n";
            stats += $"Táv átlag: {osszTav / db}\r\n";
            stats += $"Összidő: {osszIdo}\r\n";
            stats += $"Idő átlag: {atlagIdo}\r\n";

            File.WriteAllText("statisztika.txt", stats);
            Console.WriteLine(stats);
        }
    }
}
