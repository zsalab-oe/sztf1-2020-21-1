﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orai_Feladat
{
    class Tulajdonos
    {
        string nev;
        bool jogositvany;

        public bool Jogositvany { get { return jogositvany; } }

        public Tulajdonos(string nev, bool jogositvany)
        {
            this.nev = nev;
            this.jogositvany = jogositvany;
        }
    }
}
