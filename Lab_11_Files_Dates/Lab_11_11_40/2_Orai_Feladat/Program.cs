﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orai_Feladat
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto[] autok = new Auto[]
                {
                    new Auto("AAA-111", new Tulajdonos("Aladár", true)),
                    new Auto("BBB-222", new Tulajdonos("Béla", false)),
                    new Auto("CCC-333", new Tulajdonos("Cecil", true)),
                    new Auto("DDD-444", new Tulajdonos("Dénes", false)),
                    new Auto("EEE-555", new Tulajdonos("Elemér", true))
                };

            for (int i = 0; i < autok.Length; i++)
            {
                if (!autok[i].Tulajdonos.Jogositvany)
                {
                    autok[i] = null;
                }
            }
        }
    }
}
