﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orai_Feladat
{
    class Auto
    {
        string rendszam;
        Tulajdonos tulajdonos;

        public Tulajdonos Tulajdonos { get { return tulajdonos; } }

        public Auto(string rendszam, Tulajdonos tulajdonos)
        {
            this.rendszam = rendszam;
            this.tulajdonos = tulajdonos;
        }
    }
}
