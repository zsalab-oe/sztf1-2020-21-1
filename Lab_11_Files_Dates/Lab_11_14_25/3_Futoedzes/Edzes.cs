﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Futoedzes
{
    class Edzes
    {
        public DateTime Datum { get; set; }
    public double Tav { get; set; }
    public TimeSpan Ido { get; set; }


    public Edzes(string sor)
    {
        string[] adatok = sor.Split(';');
        this.Datum = DateTime.Parse(adatok[0]);
        this.Tav = double.Parse(adatok[1]);
        this.Ido = TimeSpan.Parse(adatok[2]);
    }

    public Edzes()
    {

    }

    public string Adatok()
    {
        return $"{Datum};{Tav};{Ido}";
    }
}
}
