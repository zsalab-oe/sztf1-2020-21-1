﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Futoedzes
{
    class EdzesKezelo
    {
        string futonaplo;

        public EdzesKezelo(string futonaplo)
        {
            this.futonaplo = futonaplo;
        }

        public void Felvetel(Edzes uj)
        {
            File.AppendAllText(futonaplo, uj.Adatok() + "\r\n");
        }

        public Edzes[] Listaz()
        {
            string[] sorok = File.ReadAllLines(futonaplo);
            Edzes[] edzesek = new Edzes[sorok.Length];
            for (int i = 0; i < sorok.Length; i++)
            {
                edzesek[i] = new Edzes(sorok[i]);
            }
            return edzesek;
        }

        public string Statisztikak()
        {
            Edzes[] edzesek = Listaz();
            double osszTav = OsszTav(edzesek);
            TimeSpan osszIdo = OsszIdo(edzesek);
            TimeSpan atlagIdo = TimeSpan.FromSeconds(osszIdo.TotalSeconds / edzesek.Length);

            string stats = "";
            stats += $"Edzések száma: {edzesek.Length}\r\n";
            stats += $"Össztáv: {osszTav}\r\n";
            stats += $"Táv átlag: {osszTav / edzesek.Length}\r\n";
            stats += $"Összidő: {osszIdo}\r\n";
            stats += $"Idő átlag: {atlagIdo}\r\n";

            File.WriteAllText("statisztika.txt", stats);
            return stats;
        }

        double OsszTav(Edzes[] edzesek)
        {
            double ossz = 0.0;
            for (int i = 0; i < edzesek.Length; i++)
            {
                ossz += edzesek[i].Tav;
            }
            return ossz;
        }

        TimeSpan OsszIdo(Edzes[] edzesek)
        {
            TimeSpan ossz = new TimeSpan();
            for (int i = 0; i < edzesek.Length; i++)
            {
                ossz += edzesek[i].Ido;
            }
            return ossz;
        }
    }
}
