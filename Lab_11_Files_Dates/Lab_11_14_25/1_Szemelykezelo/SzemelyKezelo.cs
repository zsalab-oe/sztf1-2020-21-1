﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace _1_Szemelykezelo
{
    class SzemelyKezelo
    {
        private string mappa; // elérési út

        // ctor + Tab + Tab
        public SzemelyKezelo()
        {
            mappa = Directory.GetCurrentDirectory();
        }

        // Ctrl + .
        public SzemelyKezelo(string mappa)
        {
            this.mappa = mappa;
        }

        string FajlNevGeneralas(Szemely sz)
        {
            string fajlnev = $"{sz.Vezeteknev[0]}_{sz.Keresztnev[0]}_{sz.Eletkor}.dat";
            return fajlnev;
        }

        public void Hozzaad(Szemely uj)
        {
            string eleresiUt = mappa + "\\" + FajlNevGeneralas(uj);
            string[] adatok = new string[]
            {
                uj.Vezeteknev,
                uj.Keresztnev,
                uj.Eletkor.ToString(),
                uj.Foglalkozas
            };
            File.WriteAllLines(eleresiUt, adatok);
        }

        public Szemely[] Listaz()
        {
            string[] fajlok = Directory.GetFiles(mappa, "*.dat");
            Szemely[] szemelyek = new Szemely[fajlok.Length];
            for (int i = 0; i < fajlok.Length; i++)
            {
                string[] adatok = File.ReadAllLines(fajlok[i]);
                Szemely sz = new Szemely();
                sz.Vezeteknev = adatok[0];
                sz.Keresztnev = adatok[1];
                sz.Eletkor = int.Parse(adatok[2]);
                sz.Foglalkozas = adatok[3];

                szemelyek[i] = sz;
            }
            return szemelyek;
        }

        public void Torol(Szemely torlendo)
        {
            string eleresiUt = this.mappa + "\\" + FajlNevGeneralas(torlendo);
            File.Delete(eleresiUt);
        }
    }
}
