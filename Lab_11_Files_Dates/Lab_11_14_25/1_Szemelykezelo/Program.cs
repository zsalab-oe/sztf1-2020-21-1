﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Szemelykezelo
{
    class Program
    {
        static void Main(string[] args)
        {
            SzemelyKezelo kezelo = new SzemelyKezelo();
            Menu(kezelo);
        }

        static void Menu(SzemelyKezelo kezelo)
        {
            int valasztott;
            do
            {
                Console.Clear();
                MenuKiir();
                valasztott = int.Parse(Console.ReadLine());
                if (valasztott == 1)
                {
                    Listazas(kezelo);
                }
                else if (valasztott == 2)
                {
                    Felvetel(kezelo);
                }
                else if (valasztott == 3)
                {
                    Torles(kezelo);
                }
                Console.WriteLine("A továbblépéshez nyomj ENTER-t!");
                Console.ReadLine();
            } while (valasztott != 4);
        }

        static void Torles(SzemelyKezelo kezelo)
        {
            Szemely[] szemelies = kezelo.Listaz();
            for (int i = 0; i < szemelies.Length; i++)
            {
                Console.WriteLine($"[{i}] {szemelies[i].Vezeteknev} {szemelies[i].Keresztnev}");
            }
            Console.WriteLine("Melyiket szeretnéd törölni? (index)");
            int torlendo = int.Parse(Console.ReadLine());
            kezelo.Torol(szemelies[torlendo]);
        }

        static void Felvetel(SzemelyKezelo kezelo)
        {
            Szemely uj = new Szemely();

            Console.Write("Vezetéknév: ");
            uj.Vezeteknev = Console.ReadLine();
            Console.Write("Keresztnev: ");
            uj.Keresztnev = Console.ReadLine();
            Console.Write("Életkor: ");
            uj.Eletkor = int.Parse(Console.ReadLine());
            Console.Write("Foglalkozás: ");
            uj.Foglalkozas = Console.ReadLine();

            kezelo.Hozzaad(uj);
        }

        static void Listazas(SzemelyKezelo kezelo)
        {
            Szemely[] szemelies = kezelo.Listaz();
            for (int i = 0; i < szemelies.Length; i++)
            {
                Console.WriteLine($"{szemelies[i].Vezeteknev} {szemelies[i].Keresztnev}");
            }
        }

        static void MenuKiir()
        {
            string menu = "";
            menu += "1: Listázás\n";
            menu += "2: Felvétel\n";
            menu += "3: Törlés\n";
            menu += "4: Kilépés\n";
            Console.WriteLine(menu);
        }
    }
}
