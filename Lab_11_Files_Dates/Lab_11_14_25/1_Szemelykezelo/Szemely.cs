﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Szemelykezelo
{
    class Szemely
    {
        public string Vezeteknev { get; set; }
        public string Keresztnev { get; set; }
        // prop + Tab + Tab
        public int Eletkor { get; set; }
        public string Foglalkozas { get; set; }
    }
}
