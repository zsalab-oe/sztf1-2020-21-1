﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_05_Kincsvadaszok
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] terkep = TerkepGeneral();
            //Console.WriteLine(TerkepSzovegkent(terkep));
            TerkepKiir(terkep);

            Console.WriteLine("---------");
            Console.WriteLine(terkep);

            int[] tomb = new int[] { 6, -5, 123 };
            Console.WriteLine(tomb[1]);
            Console.WriteLine(tomb.ToString());


            Console.ReadLine();
        }

        static int[,] TerkepGeneral()
        {
            Random rnd = new Random();
            int[,] matrix = new int[5, 5];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (rnd.Next(0, 5) < 1)
                        matrix[i, j] = rnd.Next(50, 201);
                    else
                        matrix[i, j] = 0;
                }
            }
            return matrix;
        }

        static string TerkepSzovegkent(int[,] terkep)
        {
            string s = "";
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    s += terkep[i, j] + "\t";
                }
                s += "\n";
            }
            return s;
        }

        static void TerkepKiir(int[,] terkep)
        {
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    Console.Write(terkep[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
