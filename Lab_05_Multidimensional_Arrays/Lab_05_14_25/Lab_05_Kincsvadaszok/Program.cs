﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_05_Kincsvadaszok
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] terkep = TerkepGeneral();
            //Console.WriteLine(TerkepSzovegkent(terkep));
            TerkepKiir(terkep);

            //Console.WriteLine("---------");
            //Console.WriteLine(terkep);
            //int[] tomb = new int[] { 6, -5, 123 };
            //Console.WriteLine(tomb[1]);
            //Console.WriteLine(tomb.ToString());

            Console.WriteLine(VanKulonlegesKincs(terkep));
            int[] indexek = KulonlegesKincsHelye(terkep);
            Console.WriteLine("Keresés eredménye:");
            if (indexek[0] != -1)
                Console.WriteLine($"sor: {indexek[0]}, oszlop: {indexek[1]}");
            else
                Console.WriteLine("Nem talált");

            Console.ReadLine();
        }

        static int[,] TerkepGeneral()
        {
            Random rnd = new Random();
            int[,] matrix = new int[5, 5];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (rnd.Next(0, 5) < 1)
                        matrix[i, j] = rnd.Next(50, 201);
                    else
                        matrix[i, j] = 0;
                }
            }
            return matrix;
        }

        static string TerkepSzovegkent(int[,] terkep)
        {
            string s = "";
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    s += terkep[i, j] + "\t";
                }
                s += "\n";
            }
            return s;
        }

        static void TerkepKiir(int[,] terkep)
        {
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    Console.Write(terkep[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

        static int OsszErtek(int[,] terkep)
        {
            int ossz = 0;
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    ossz += terkep[i, j];
                }
            }
            return ossz;
        }

        static int KincsekSzama(int[,] terkep)
        {
            int db = 0;
            for (int i = 0; i < terkep.GetLength(0); i++)
            {
                for (int j = 0; j < terkep.GetLength(1); j++)
                {
                    if (terkep[i, j] != 0)
                        db++;
                }
            }
            return db;
        }

        static bool VanKulonlegesKincs(int[,] terkep)
        {
            int sor = 0;
            int oszlop = 0;
            bool van = false;
            while (!van && sor < terkep.GetLength(0))
            {
                while (oszlop < terkep.GetLength(1) &&
                    terkep[sor, oszlop] < 150)
                {
                    oszlop++;
                }
                if (oszlop < terkep.GetLength(1))
                {
                    van = true;
                }
                else
                {
                    sor++;
                    oszlop = 0;
                }
            }
            return van;
        }

        static bool VanKulonlegesKincsRondaMego(int[,] terkep)
        {
            for (int sor = 0; sor < terkep.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < terkep.GetLength(1); oszlop++)
                {
                    if (terkep[sor, oszlop] >= 150)
                        return true;
                }
            }
            return false;
        }

        static bool VanKulonlegesKincsRondaEsNemIsOptimalisMego(int[,] terkep)
        {
            bool van = false;
            for (int sor = 0; sor < terkep.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < terkep.GetLength(1); oszlop++)
                {
                    if (terkep[sor, oszlop] >= 150)
                        van = true;
                }
            }
            return van;
        }


        static int[] KulonlegesKincsHelye(int[,] terkep)
        {
            int sor = 0;
            int oszlop = 0;
            bool van = false;
            while (!van && sor < terkep.GetLength(0))
            {
                while (oszlop < terkep.GetLength(1) &&
                    terkep[sor, oszlop] < 150)
                {
                    oszlop++;
                }
                if (oszlop < terkep.GetLength(1))
                {
                    van = true;
                }
                else
                {
                    sor++;
                    oszlop = 0;
                }
            }
            if (van)
                return new int[] { sor, oszlop };
            else
                return new int[] { -1, -1 };
        }
    }
}
