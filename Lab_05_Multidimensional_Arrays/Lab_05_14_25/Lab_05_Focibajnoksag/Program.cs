﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_05_Focibajnoksag
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] csapatok = { "Arsenal", "Brighton", "Chelsea", "Derby County", "Everton", "Fulham" };

            int[,] eredmenyek = EredmenyekFeltolt(csapatok.Length);

            Console.WriteLine(EredmenyekSzovegkent(eredmenyek));

            Console.WriteLine(OsszPont(eredmenyek, 4));

            int[] osszesitett = OsszesitettEredmenyek(eredmenyek);
            Console.WriteLine("---------");
            Console.WriteLine(Szovegkent(csapatok, osszesitett));
            Rendez(csapatok, osszesitett);
            Console.WriteLine();
            Console.WriteLine(Szovegkent(csapatok, osszesitett));


            Console.ReadLine();
        }

        static int[,] EredmenyekFeltolt(int csapatokSzama)
        {
            Random rnd = new Random();
            int[,] eredmenyek = new int[csapatokSzama, csapatokSzama];
            for (int i = 0; i < csapatokSzama; i++)
            {
                for (int j = 0; j < csapatokSzama; j++)
                {
                    if (i != j)
                    {
                        int e = rnd.Next(0, 3);
                        if (e < 2)
                            eredmenyek[i, j] = e;
                        else
                            eredmenyek[i, j] = 3;
                    }
                }
            }
            return eredmenyek;
        }

        static string EredmenyekSzovegkent(int[,] eredmenyek)
        {
            string s = "";
            for (int i = 0; i < eredmenyek.GetLength(0); i++)
            {
                for (int j = 0; j < eredmenyek.GetLength(1); j++)
                {
                    s += eredmenyek[i, j] + " ";
                }
                s += "\n";
            }
            return s;
        }

        static int CsapatKeres(string[] csapatok, string keresett)
        {
            int j = 0;
            while (j < csapatok.Length && csapatok[j] != keresett)
                j++;
            if (j < csapatok.Length)
                return j;
            else
                return -1;
        }

        static int OsszPont(int[,] eredmenyek, int csapatIndex)
        {
            int ossz = 0;
            // hazai meccsek:
            for (int j = 0; j < eredmenyek.GetLength(1); j++)
            {
                if (j != csapatIndex)
                {
                    ossz += eredmenyek[csapatIndex, j];
                }
            }
            // vendég meccsek:
            for (int i = 0; i < eredmenyek.GetLength(0); i++)
            {
                if (i != csapatIndex)
                {
                    if (eredmenyek[i, csapatIndex] == 0)
                        ossz += 3;
                    else if (eredmenyek[i, csapatIndex] == 1)
                        ossz += 1;
                }
            }
            return ossz;
        }
        static int[] OsszesitettEredmenyek(int[,] eredmenyek)
        {
            int[] osszesitett = new int[eredmenyek.GetLength(0)];
            for (int i = 0; i < osszesitett.Length; i++)
            {
                osszesitett[i] = OsszPont(eredmenyek, i);
            }
            return osszesitett;
        }

        static string Szovegkent(string[] csapatok, int[] osszesitettEredmenyek)
        {
            string s = "";
            for (int i = 0; i < csapatok.Length; i++)
            {
                s += $"{csapatok[i]}\t({osszesitettEredmenyek[i]}p)\n";
            }
            return s;
        }

        static void Rendez(string[] csapatok, int[] osszesitettEredmenyek)
        {
            for (int i = 0; i < osszesitettEredmenyek.Length - 1; i++)
            {
                int maxIndex = i;
                for (int j = i + 1; j < osszesitettEredmenyek.Length; j++)
                {
                    if (osszesitettEredmenyek[j] > osszesitettEredmenyek[maxIndex])
                        maxIndex = j;
                }
                Csere(ref osszesitettEredmenyek[maxIndex], ref osszesitettEredmenyek[i]);
                Csere(ref csapatok[maxIndex], ref csapatok[i]);
            }
        }

        static void Csere(ref int elso, ref int masodik)
        {
            int seged = elso;
            elso = masodik;
            masodik = seged;
        }

        static void Csere(ref string elso, ref string masodik)
        {
            string seged = elso;
            elso = masodik;
            masodik = seged;
        }
    }
}
