﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_6
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott adatok alapján feltölt egy 10 elemű egész szám tömböt, majd a képernyőre kiírja, hogy hányadik volt a legkisebb és legnagyobb elem, valamint mennyi volt ezeknek az értéke.

            int[] szamok = new int[10];
            for (int i = 0; i < szamok.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. számot: ");
                int megadottSzam = int.Parse(Console.ReadLine());
                szamok[i] = megadottSzam;
            }
            Console.WriteLine();


            /* Minimumkiválasztás tétel */
            int minIndex = 0;
            for (int i = 1; i < szamok.Length; i++)
            {
                if (szamok[i] < szamok[minIndex])
                    minIndex = i;
            }
            Console.WriteLine("A legkisebb elem indexe: " + minIndex);
            Console.WriteLine("A legkisebb elem: " + szamok[minIndex]);


            /* Maximumkiválasztás tétel */
            int maxIndex = 0;
            for (int i = 1; i < szamok.Length; i++)
            {
                if (szamok[i] > szamok[maxIndex])
                    maxIndex = i;
            }
            Console.WriteLine("A legnagyobb elem indexe: " + maxIndex);
            Console.WriteLine("A legnagyobb elem: " + szamok[maxIndex]);

            // Megjegyzés: ha a legkisebb vagy a legnagyobb elem többször is szerepel a tömbben, akkor azok közül az elsőt adja meg megoldásként


            Console.ReadLine();
        }
    }
}
