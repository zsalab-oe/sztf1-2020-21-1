﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott adatok alapján feltölt egy 10 elemű egész szám tömböt, majd a képernyőre kiírja, hogy van-e közöttük páratlan szám.


            int[] szamok = new int[10];
            for (int i = 0; i < szamok.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. számot: ");
                int megadottSzam = int.Parse(Console.ReadLine());
                szamok[i] = megadottSzam;
            }

            /* Eldöntés tétel */
            int j = 0;
            while (j < szamok.Length && szamok[j] % 2 == 0)
            {
                j++;
            }

            bool van = j < szamok.Length;
            if (van)
            {
                Console.WriteLine("A megadott szamok között van páratlan szám.");
            }
            else
            {
                Console.WriteLine("A megadott szamok között nincs páratlan szám.");
            }


            Console.ReadLine();
        }
    }
}
