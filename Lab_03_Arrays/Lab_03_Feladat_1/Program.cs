﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott adatok alapján feltölt egy 10 elemű egész szám tömböt, majd a képernyőre kiírja, hogy közöttük hány 0-ra végződő szám van.


            int[] szamok = new int[10];
            for (int i = 0; i < szamok.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. számot: ");
                int megadottSzam = int.Parse(Console.ReadLine());
                szamok[i] = megadottSzam;
            }


            /* Megszámlálás tétel */
            int db = 0;
            for (int i = 0; i < szamok.Length; i++)
            {
                if (szamok[i] % 10 == 0)
                {
                    db++;
                }
            }

            Console.WriteLine($"A megadott számok között {db} db 0-ra végződő szám van.");


            Console.ReadLine();
        }
    }
}
