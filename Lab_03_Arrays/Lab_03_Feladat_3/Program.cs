﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott adatok alapján feltölt egy 10 elemű egész szám tömböt. Ha tudjuk, hogy szerepel a tömbben páros szám, akkor mi az első ilyen szám indexe?

            int[] szamok = new int[10];
            for (int i = 0; i < szamok.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. számot: ");
                int megadottSzam = int.Parse(Console.ReadLine());
                szamok[i] = megadottSzam;
            }


            /* Kiválasztás tétel */
            int j = 0;
            while (szamok[j] % 2 == 1)
            {
                j++;
            }
            Console.WriteLine("Első páros elem indexe: " + j);


            Console.ReadLine();
        }
    }
}
