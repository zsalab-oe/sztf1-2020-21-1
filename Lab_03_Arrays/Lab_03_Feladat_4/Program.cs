﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott szavak alapján feltölt egy 8 elemű egydimenziós tömböt, majd ezeket a szavakat visszafelé összefűzi egy mondattá.

            string[] szavak = new string[8];
            for (int i = 0; i < szavak.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. szót: ");
                string megadottSzo = Console.ReadLine();
                szavak[i] = megadottSzo;
            }


            /* Sorozatszámítás tétel */
            string mondat = ""; // üres string
            for (int i = 0; i < szavak.Length; i++)
            {
                mondat = szavak[i] + " " + mondat;
            }

            Console.WriteLine(mondat);


            // másik lehetőség:
            string mondat2 = "";
            //                        vagy: i > -1
            for (int i = szavak.Length - 1; i >= 0; i--)
            {
                mondat2 += szavak[i] + " ";
            }

            
            Console.WriteLine(mondat2);


            Console.ReadLine();
        }
    }
}
