﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Bevezeto
{
    class Program
    {
        static void Main(string[] args)
        {
            //int szam = 10;  // 8    4   -2
            //int j = 0;      // 2    4   6
            //while (j < 5)
            //{
            //    j = j + 2;
            //    szam = szam - j;
            //}
            //Console.WriteLine(szam);

            //int x = 2;  // 3    4
            //int y = ++x + x++;
            ////    =  3  + 3
            //Console.WriteLine(y);


            /* tömbök */

            // deklaráció:
            // típus[] tömbnév;
            int[] tomb;

            // tömblétrehozás:
            // tömbnév = new típus[elemszám];
            tomb = new int[10];

            // Hivatkozás a tömb egy elemére:
            //     tömbnév[index]
            // ahol index egy nemnegatív egész szám (index >= 0)
            tomb[2] = 1000;
            // Ertékadás hiányában a tömbelem a típus alapértékét veszi fel Ez int típusnál: 0

            // A tömb első elemének indexe: 0
            // A tömb utolsó elemének indexe: elemszám - 1
            // Kisebb, vagy nagyobb index megadása futási hibát okoz
            Console.WriteLine("A tömb első eleme: " + tomb[0]);
            Console.WriteLine("A tömb utolsó eleme: " + tomb[tomb.Length - 1]);



            /* for ciklus lehetőségek */

            for (int i = 0; i < tomb.Length; i++)
            {
                tomb[i] = 10 * i + i;
            }

            for (int i = 0; i < tomb.Length; i++)
            {
                Console.WriteLine(tomb[i]);
            }

            Console.WriteLine("----------");

            for (int i = 0, k = 0; i < tomb.Length; i = i + 2, k += 4)
            {
                Console.WriteLine(tomb[i]);
            }

            Console.WriteLine("----------");

            for (int i = tomb.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(tomb[i]);
            }




            Console.WriteLine("----------");

            for (int i = 0, j = 0; i < tomb.Length; i++, j+= 2)
            {
                Console.Write(tomb[i] + "\t");
                Console.WriteLine(tomb[j % tomb.Length]);
            }


            Console.ReadLine();
        }
    }
}
