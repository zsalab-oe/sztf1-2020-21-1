﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_7
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsen egy konzolos alkalmazást, amelyben egy 20 elemű egész számokból álló tömböt feltölt 0 és 100 közötti véletlen számokkal. Feladatok:
            //     a. Adja meg a páratlan számok átlagát!
            //     b. Határozza meg, hogy szerepel-e a tömbben egyjegyű szám, és ha igen, adja vissza az első előfordulásának indexét!
            //     c. Adja meg a legnagyobb páros szám első előfordulásának indexét és értékét!


            Random rnd = new Random();
            int[] szamok = new int[20];
            for (int i = 0; i < szamok.Length; i++)
            {
                szamok[i] = rnd.Next(0, 101);
            }

            for (int i = 0; i < szamok.Length; i++)
            {
                Console.Write(szamok[i] + "   ");
            }
            Console.WriteLine();



            /* a. feladat (sorozatszámítás tétel) */
            int osszeg = 0;
            int paratlanDb = 0;
            for (int i = 0; i < szamok.Length; i++)
            {
                if (szamok[i] % 2 == 1)
                {
                    osszeg += szamok[i];
                    paratlanDb++;
                }
            }
            double planAtlag = (double)osszeg / paratlanDb;
            Console.WriteLine($"A páratlan számok átlaga: {planAtlag}");


            /* b. feladat (lineáris keresés tétel) */
            int j = 0;
            while (j < szamok.Length && szamok[j] > 9)
            {
                j++;
            }
            if (j < szamok.Length)
            {
                Console.WriteLine($"Az első egyjegyű szám indexe: {j}");
            }
            else
            {
                Console.WriteLine($"A tömbben nem szerepel egyjegyű szám.");
            }



            /* c. feladat (összetett feltételű maximumkiválasztás) */

            // Ebben az esetben összetett feltételünk van: csak a páros elemek közül keressük a legnagyobb elem helyét!
            // Most nem jelölhetem ki a tömb első elemét, mint eddig talált legnagyobb elemet, mert nem lehetünk benne biztosak, hogy megfelel a másik feltételünknek is (esetünkben, hogy 2-vel osztható, sőt abban sem lehetünk biztosak, hogy van egyáltalán 2-vel osztható elem a tömbben), ezért egyelőre egy nem érvényes index értéket tárolunk a maxIndex változóban.

            int maxIndex = -1;
            int maxErtek = int.MinValue; // int típusban eltárolható legkisebb érték
            for (int i = 0; i < szamok.Length; i++)
            {
                if (szamok[i] % 2 == 0 && szamok[i] > maxErtek)
                {
                    maxIndex = i;
                    maxErtek = szamok[i];
                }
            }

            Console.WriteLine("A legnagyobb páros elem indexe (-1, ha nem volt páros elem): " + maxIndex);
            if (maxIndex >= 0)
                Console.WriteLine("A legnagyobb páros elem értéke: " + szamok[maxIndex]);


            Console.ReadLine();
        }
    }
}
