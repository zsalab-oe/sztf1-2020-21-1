﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_03_Feladat_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // Készítsünk programot, amely billentyűzetről megadott szavak alapján feltölt egy 8 elemű egydimenziós tömböt, majd megmondja egy szintén billentyűzetről megadott szóról, hogy szerepel-e a tömbben, és ha igen, adja vissza az első előfordulásának sorszámát.

            string[] szavak = new string[8];
            for (int i = 0; i < szavak.Length; i++)
            {
                Console.Write($"Add meg a(z) {i + 1}. szót: ");
                string megadottSzo = Console.ReadLine();
                szavak[i] = megadottSzo;
            }
            
            Console.Write($"Add meg a keresett szót: ");
            string keresettSzo = Console.ReadLine();


            /* Lineáris keresés tétel */
            int j = 0;
            while (j < szavak.Length && szavak[j] != keresettSzo)
            {
                j++;
            }

            if (j < szavak.Length)
            {
                Console.WriteLine($"A keresett szó sorszáma: {j + 1}.");
            }
            else
            {
                Console.WriteLine("A keresett szó nem szerepel a tömbben.");
            }


            Console.ReadLine();
        }
    }
}
