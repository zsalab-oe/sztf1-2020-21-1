﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orszag_Varos
{
    class Orszag
    {
        /* adattagok */

        string nev;
        int nepesseg;
        double terulet;
        Varos fovaros;
        string hivatalosNyelv;
        string penznem;
        decimal gdp;



        /* tulajdonságok */

        public string Nev
        {
            get { return nev; }
            set { nev = value; }
        }

        public int Nepesseg
        {
            get { return nepesseg; }
            set { nepesseg = value; }
        }

        public string Penznem
        {
            get { return penznem; }
            set
            {
                if (ErvenyesPenznem(value)) // adat ellenőrzésése kifejezetten alkalmas a set blokk
                    penznem = value.ToUpper();
            }
        }

        public decimal GDP
        {
            get { return gdp; }  // gyakori hiba, hogy a getter/setter blokkon belül magára a tulajdonságra hivatkozunk az adattag helyett (--> StackOverflowException)
            set
            {
                if (value > gdp)
                    gdp = value;
            }
        }

        public Varos Fovaros // nincs set metódus --> csak olvasható tulajdonság
        {
            get { return fovaros; }
        }

        public double Terulet
        {
            get { return terulet; }
        }

        public string HivatalosNyelv
        {
            get { return hivatalosNyelv; }
            private set { hivatalosNyelv = value; }
        }

        public double Nepsuruseg // önálló tulajdonság
        {
            get
            {
                return nepesseg * 1000000 / (double)terulet;
            }
        }

        public decimal EgyForeJutoGDP
        {
            get { return this.gdp / nepesseg; }
        }



        /* konstruktorok */

        public Orszag(string nev, int nepesseg, double terulet, Varos fovaros, string hivatalosNyelv, string penznem, decimal gdp)
        {
            this.nev = nev;
            this.nepesseg = nepesseg;
            this.terulet = terulet;
            this.fovaros = fovaros;
            this.hivatalosNyelv = hivatalosNyelv;
            this.penznem = penznem;
            this.gdp = gdp;
        }

        public Orszag()
        {

        }




        /* metódusok */

        bool ErvenyesPenznem(string penznem)
        {
            if (penznem.Length != 3)
                return false;

            int j = 0;
            while (j < penznem.Length && char.IsLetter(penznem[j]))
            {
                j++;
            }
            return j == penznem.Length;
        }

        public double Hanyszoros()
        {
            return this.terulet / fovaros.Terulet;
        }
    }
}
