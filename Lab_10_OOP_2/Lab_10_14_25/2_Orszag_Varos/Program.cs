﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orszag_Varos
{
    class Program
    {
        static void Main(string[] args)
        {
            Varos budapest = new Varos("Budapest", 1752286, 525.13);
            

            Orszag magyarorszag = new Orszag("Magyarország", 10, 93000, budapest, "magyar", "HUF", 157861);

            budapest.Orszag = magyarorszag;
            //budapest.OrszagHozzarendel(magyarorszag); // vagy így

            magyarorszag.GDP = 180000;
            Console.WriteLine(magyarorszag.GDP);
            //magyarorszag.Terulet = 73000; // hibás, csak olvasható
            Console.WriteLine(magyarorszag.HivatalosNyelv); // OK
            //magyarorszag.HivatalosNyelv = "enyelv"; // not OK


            Console.WriteLine(magyarorszag.Hanyszoros());


            Console.WriteLine(magyarorszag.Terulet / magyarorszag.Fovaros.Terulet);

            Console.WriteLine(magyarorszag.Terulet / magyarorszag.Fovaros.Orszag.Fovaros.Orszag.Fovaros.Orszag.Fovaros.Terulet);



            Console.ReadLine();
        }
    }
}
