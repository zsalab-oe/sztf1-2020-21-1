﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Orszag_Varos
{
    class Varos
    {
        // automatikus tulajdonságok
        // - getter és setter blokkokban nincs lehetőség kód írására
        // - el is hagyható például a setter blokk (ebben az esetben a tulajdonság csak a konstruktorban kaphat értéket)
        public string Nev { get; set; }
        public int Nepesseg { get; set; }

        // a get és set láthatósági jelzője eltérhet
        // - ebben az esetben a tulajdonság osztályon belülről írható és olvasható, kívülről (másik osztályokból) csak olvasható
        public Orszag Orszag { get; private set; }
        public double Terulet { get; private set; }


        public Varos(string nev, int nepesseg, Orszag orszag, double terulet)
        {
            Nev = nev;
            Nepesseg = nepesseg;
            Orszag = orszag;
            Terulet = terulet;
        }

        public Varos(string nev, int nepesseg, double terulet)
        {
            Nev = nev;
            Nepesseg = nepesseg;
            Terulet = terulet;
        }

        public Varos()
        {

        }

        public void OrszagHozzarendel(Orszag orszag)
        {
            this.Orszag = orszag;
        }
    }
}
