﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Tulajdonsagok
{
    //  **** LÁTHATÓSÁG ****

    // - a láthatóság határozza meg, hogy az osztály egyes tagjaira milyen széles körben hivatkozhatunk
    // PRIVATE: a privát láthatóság a legszigorúbb, ezzel a jelzővel ellátott tagok csak az osztályon belül hivatkozhatóak (tehát ebben az esetben a class Hallgato { } kódblokkjában), vagyis egy másik osztályból nem férünk hozzá ezekhez az adatokhoz
    // PUBLIC: publikus láthatóság, vagyis nincs megkötés arra vonatkozóan, honnan férhetünk hozzá az adathoz
    // vannak egyéb láthatósági jelzők is, ezekkel későbbi félévekben foglalkozunk
    // - amennyiben mi magunk nem adunk meg láthatóságot módosító kulcsszót, úgy az alapértelmezett láthatósági szint kerül használatba
    // - osztály tagjainak alapértelmezett láthatósága: PRIVATE



    // **** TULAJDONSÁGOK ****

    // az adattagokat tipikusan nem tesszük kívülről elérhetővé, ezért azok elérését, módosítását tulajdonságokon keresztül biztosítjuk
    // get és set kulcsszavakkal jelölt speciális hozzáférési metódusokból állhat
    // - a get az olvasáskor, a set az íráskor lefutó műveleteket tartalmazza
    // - a set vagy a get metódus elhagyható, ebben az esetben az adott tulajdonságot nem tudjuk írni vagy olvasni
    // - a get és set láthatósági jelzője eltérhet
    // - előnye: az olvasás és írás jogot külön adhatjuk meg

    // másképp fogalmazva: a tulajdonság egy hozzáférést szabályzó réteg az adattag előtt

    // Az adatrejtés elve szerint mindig csak azoknak az adattagoknak szélesítjük a hozzáférhetőségét, amelyek megkövetelik.
    // FIGYELEM: Amennyiben egy adattag indokolatlanul publikus, vagy indokolatlanul készül tulajdonság hozzá, HIBÁNAK számít!


    class Szemely
    {
        // privát adattag
        string nev;

        // publikus hozzáférést segítő metódusok
        public string GetNev()
        {
            return nev;
        }
        public void SetNev(string value)
        {
            nev = value;
        }

        // publikus hozzáférést segítő tulajdonság
        public string Nev   // a tulajdonság elnevezése gyakran az adatmező nevének nagy kezdőbetűs változata (nincs zárójel!)
        {
            get // a get egy speciális kiolvasó metódus
            {
                return nev;
            }
            set // a set egy speciális író metódus
            {
                nev = value; // value: az átadott érték (rejtett paraméter)
            }
        }

        public string Lakcim { get; set; }

        

        int eletkor;
        public int Eletkor
        {
            get
            {
                return eletkor;
            }
        }
        public int GetEletkor()
        {
            return eletkor;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Szemely geza = new Szemely();
            // értékadás, érték lekérdezése metódusokon keresztül
            geza.SetNev("Géza");
            Console.WriteLine(geza.GetNev());

            // értékadás, érték lekérdezése tulajdonságon keresztül
            geza.Nev = "Mézga Géza";
            Console.WriteLine(geza.Nev);
        }
    }
}
