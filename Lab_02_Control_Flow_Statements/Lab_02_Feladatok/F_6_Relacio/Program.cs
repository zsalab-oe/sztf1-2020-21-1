﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_6_Relacio
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("a: ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("b: ");
            double b = double.Parse(Console.ReadLine());
            char relaciosJel;
            if (a < b)
                relaciosJel = '<';
            else if (a > b)
                relaciosJel = '>';
            else
                relaciosJel = '=';

            Console.WriteLine($"{a} {relaciosJel} {b}");

            Console.ReadLine();
        }
    }
}
