﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_4_Hatvany_Faktorialis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write(">> Hatványozás\na: "); // '\n': sortörés karakter
            int alap = int.Parse(Console.ReadLine());
            Console.Write("n: ");
            int kitevo = int.Parse(Console.ReadLine());
            long eredm = 1;
            int l = 1;
            while (l <= kitevo)
            {
                eredm *= alap;
                l++;
            }
            Console.WriteLine($"{alap}^{kitevo} = {eredm}");

            Console.Write("\n>> Faktoriális\nn: ");

            int n = int.Parse(Console.ReadLine());
            long eredmFakt = 1;
            int k = 2;
            while (k <= n)
            {
                eredmFakt *= k;
                k++;
            }
            Console.WriteLine($"{n}! = {eredmFakt}");

            Console.ReadLine();
        }
    }
}
