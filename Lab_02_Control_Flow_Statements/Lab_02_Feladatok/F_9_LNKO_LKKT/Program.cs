﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_9_LNKO_LKKT
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("m: ");
            int m = int.Parse(Console.ReadLine());
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());

            /* LNKO */
            int a = m;
            int b = n;
            int maradek;
            do
            {
                maradek = a % b;
                a = b;
                b = maradek;
            }
            while (maradek != 0);
            int lnko = a;
            Console.WriteLine("LNKO: " + lnko);

            /* LKKT */
            int lkkt = (m / lnko) * n;
            Console.WriteLine("LKKT: " + lkkt);

            Console.ReadLine();
        }
    }
}
