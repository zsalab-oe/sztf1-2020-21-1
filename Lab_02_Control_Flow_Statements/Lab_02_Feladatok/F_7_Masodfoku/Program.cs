﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_7_Masodfoku
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("a: ");
            double a = double.Parse(Console.ReadLine());

            Console.Write("b: ");
            double b = double.Parse(Console.ReadLine());

            Console.Write("c: ");
            double c = double.Parse(Console.ReadLine());

            double D = Math.Pow(b, 2) - 4 * a * c;

            if (D > 0)
            {
                double x1 = (-b + Math.Sqrt(D)) / (2 * a);
                double x2 = (-b - Math.Sqrt(D)) / (2 * a);

                Console.WriteLine("Az egyenletnek 2 megoldása van:");
                Console.WriteLine($"x1 = {x1}");
                Console.WriteLine($"x2 = {x2}");
            }
            else if (D == 0)
            {
                double x1 = (-b + Math.Sqrt(D) / 2 * a);

                Console.WriteLine("Az egyenletnek 1 megoldása van:");
                Console.WriteLine($"x = {x1}");
            }
            else
            {
                Console.WriteLine("Az egyenletnek nincs megoldása a valós számok halmazán!");
            }

            Console.ReadLine();
        }
    }
}
