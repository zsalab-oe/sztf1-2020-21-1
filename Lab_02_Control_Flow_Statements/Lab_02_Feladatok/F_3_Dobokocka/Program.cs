﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F_3_Dobokocka
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random(); // Random típusú segédváltozó létrehozása

            Console.Write($"Dobások száma: ");
            int dobasokSzama = int.Parse(Console.ReadLine());
            int szamlalo = 0;

            int hatosDb = 0;
            int parosDb = 0;
            while (szamlalo < dobasokSzama)
            {
                int ertek = rnd.Next(1, 7); // véletlenszám előállítása
                                            // [1, 7[ => 1, 2, 3, 4, 5, 6
                Console.WriteLine($"{szamlalo + 1}. dobás eredménye: {ertek}");
                if (ertek == 6)
                {
                    hatosDb++;
                }
                if (ertek % 2 == 0)
                {
                    parosDb++;
                }
                szamlalo++;
            }

            Console.WriteLine($"Hatos: {hatosDb} db");
            Console.WriteLine($"Páros: {parosDb} db");

            Console.ReadLine();
        }
    }
}
