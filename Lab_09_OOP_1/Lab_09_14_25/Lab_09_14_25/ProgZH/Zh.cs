﻿using System;

namespace _3_ProgZH
{
    class Zh
    {
        public string neptunkod;
        public int pontszam;

        public Zh(Random rnd)
        {
            this.neptunkod = NeptunkodGeneralas(rnd);
            this.pontszam = rnd.Next(0, 101);
        }

        string NeptunkodGeneralas(Random rnd)
        {
            string neptunkod = "";
            for (int i = 0; i < 6; i++)
            {
                if (rnd.Next(0, 10) < 3)
                    neptunkod += rnd.Next(0, 10).ToString();
                else
                    neptunkod += (char)rnd.Next('A', 'Z' + 1);
            }
            return neptunkod;
        }

        public override string ToString()
        {
            return $"{neptunkod}:   {pontszam}p";
        }
    }
}
