﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace _3_ProgZH
{
    class Program
    {
        static void Main(string[] args)
        {
            Zh[] zhk = new Zh[20];
            Feltolt(zhk);
            string[] atmentek = Atmentek(zhk);
            for (int i = 0; i < atmentek.Length; i++)
            {
                Console.WriteLine(atmentek[i]);
            }
        }

        static void Feltolt(Zh[] zhk)
        {
            Random rnd = new Random();
            for (int i = 0; i < zhk.Length; i++)
            {
                zhk[i] = new Zh(rnd);
            }
        }

        static string[] Atmentek(Zh[] zhk)
        {
            string atmentek = "";
            for (int i = 0; i < zhk.Length; i++)
            {
                if (zhk[i].pontszam >= 50)
                {
                    //Console.WriteLine(zhk[i].neptunkod);
                    atmentek += zhk[i].neptunkod + "*";
                }
            }
            atmentek = atmentek.TrimEnd('*');
            return atmentek.Split('*');
        }
    }
}
