﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Allatok
{
    class Kutya
    {
        private string nev;
        private string szin;
        private int eletkor;
        int elragcsaltCsontokSzama; // alapértelmezetten: private
        bool hullikASzore;

        public Kutya(string nev, string szin, int eletkor, int elragcsaltCsontokSzama, bool hullikASzore)
        {
            this.nev = nev;
            this.szin = szin;
            this.eletkor = eletkor;
            this.elragcsaltCsontokSzama = elragcsaltCsontokSzama;
            this.hullikASzore = hullikASzore;
        }

        public void Eszik(int csont)
        {
            if (csont >= 3)
            {
                Ugat();
            }
            elragcsaltCsontokSzama += csont;
        }

        void Ugat() // alapértelmezetten: private
        {
            Console.WriteLine("Vaú");
        }
    }
}
