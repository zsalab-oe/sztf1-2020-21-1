﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Allatok
{
    class Program
    {
        static void Main(string[] args)
        {
            Kutya[] kutyusok = new Kutya[4];
            kutyusok[0] = new Kutya("Buksi", "barna", 4, 0, false);
            kutyusok[1] = new Kutya("Morzsa", "barna", 3, 3, true);
            kutyusok[2] = new Kutya("Hercules", "fekete", 2, 0, false);
            kutyusok[3] = new Kutya("Zsömle", "barna", 5, 2, false);

            Etet(kutyusok);

            int[] ertekek = new int[] { 1, 4, 8, 15 };

            Macska[] macskak = new Macska[]
            {
                new Macska("Cirmi", 4, 10, false),
                new Macska("Kormos", 2, 15, true),
                new Macska("Csikos", 5, 100, false),
                new Macska("Pocak", 4, 5, true),
                new Macska("Tiki", 4, 10, false)
            };

            Udvozles(macskak);



            //string[] nevek = new string[] { "Cirmi", "Kormos", "Csikos", "Pocak", "Tiki" };
            //int[] labakSzama = new int[] { 4, 2, 5, 4, 4 };
            //int[] farokHossz = new int[] { 10, 15, 100, 5, 10 };
            //bool[] rosszcsont = new bool[] { false, true, false, true, false };

            Console.ReadLine();
        }

        //static void Udvozles(string[] nevek, bool[] rosszcsont)
        //{
        //    for (int i = 0; i < rosszcsont.Length; i++)
        //    {
        //        if (rosszcsont[i])
        //        {
        //            Console.WriteLine($"Miau ({nevek[i]})");
        //        }
        //    }
        //}

        static void Udvozles(Macska[] macskak)
        {
            for (int i = 0; i < macskak.Length; i++)
            {
                if (macskak[i].rosszcsont)
                    Console.WriteLine(macskak[i].Udvozol());
            }
        }

        static void Etet(Kutya[] kutyak)
        {
            Random rnd = new Random();
            for (int i = 0; i < kutyak.Length; i++)
            {
                kutyak[i].Eszik(rnd.Next(1, 10));
            }
        }
    }
}
