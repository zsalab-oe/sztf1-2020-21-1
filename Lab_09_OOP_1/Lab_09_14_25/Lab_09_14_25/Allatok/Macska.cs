﻿namespace _2_Allatok
{
    class Macska
    {
        string nev;
        int labakSzama;
        int farokHossz;
        public bool rosszcsont;

        public int LabakSzama()
        {
            return labakSzama;
        }

        public Macska(string nev, int labakSzama, int farokHossz, bool rosszcsont)
        {
            this.nev = nev;
            this.labakSzama = labakSzama;
            this.farokHossz = farokHossz;
            this.rosszcsont = rosszcsont;
        }

        public string Udvozol()
        {
            return $"Miau ({nev})";
        }
    }
}
