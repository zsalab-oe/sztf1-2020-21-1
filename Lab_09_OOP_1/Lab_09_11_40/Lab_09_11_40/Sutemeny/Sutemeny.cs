﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_OOP_Alapok
{
    // **** OOP bevezető ****

    // OSZTÁLY
    // - Az osztály felfogható az objektumok sablonjaként (másképp: tekinthetünk rá úgy, mint egy süteményreceptre)
    // - meghatározza, hogy az osztályhoz tartozó objektumok hogyan jönnek létre, hogyan semmisülnek meg, milyen adatokat tartalmaznak, és ezek az adatok milyen módon módosíthatók

    // OBJEKTUM
    // - egy osztály egy példánya
    // - az osztály által meghatározott adatokat tartalmazza

    // Osztályok deklarálása:
    // class OsztályNeve { }
    // az osztály nevét követő kódblokkon belül határozzuk meg az osztály tagjait (egyelőre):
    // - adattagok
    // - konstruktorok
    // - metódusok

    class Sutemeny
    {
        //  **** LÁTHATÓSÁG ****

        // - a láthatóság határozza meg, hogy az osztály egyes tagjaira milyen széles körben hivatkozhatunk
        // PRIVATE: a privát láthatóság a legszigorúbb, ezzel a jelzővel ellátott tagok csak az osztályon belül hivatkozhatóak (tehát ebben az esetben a class Sutemeny { } kódblokkjában), vagyis egy másik osztályból nem férünk hozzá ezekhez az adatokhoz
        // PUBLIC: publikus láthatóság, vagyis nincs megkötés arra vonatkozóan, honnan férhetünk hozzá az adathoz
        // vannak egyéb láthatósági jelzők is, ezekkel későbbi félévekben foglalkozunk
        // - amennyiben mi magunk nem adunk meg láthatóságot módosító kulcsszót, úgy az alapértelmezett láthatósági szint kerül használatba
        // - osztály tagjainak alapértelmezett láthatósága: PRIVATE



        // **** ADATTAGOK ****

        // - változók, amelyeket osztályok esetén adattagoknak nevezünk
        // - ezekkel határozzuk meg, hogy milyen információt szeretnénk tárolni az osztályunk egy példányában
        // - minden egyes sütemény önálló értékekkel fog rendelkezni az alábbiakból

        public string nev;
        public int liszt;
        public int tojasDb;
        private bool cukormaz;



        // **** KONSTRUKTOROK ****

        // - a konstruktor egy speciális metódus, amelynek nincs visszatérési értéke (még void sem), és neve minden esetben megegyezik az osztály nevével
        // - az objektumok létrehozásakor adunk kezdőértékeket az objektumhoz tartozó adattagoknak
        // - minden példányosításnál automatikusan meghívódik (példányosítás: adott osztályból készül egy új példány / objektum)
        // - csak egyszer hívódik meg, az objektum létrehozásakor
        // - egy osztály több konstruktort is tartalmazhat, más-más paraméretekkel (metódus túlterhelés elven)
        // - ha nem hozunk létre konstruktort, akkor és csak akkor a fordító automatikusan létrehoz egy paraméter nélküli alapértelmezett konstruktort

        public Sutemeny(string nev, int liszt, int tojasDb, bool cukormaz)
        {
            this.nev = nev;
            this.liszt = liszt;
            this.tojasDb = tojasDb;
            this.cukormaz = cukormaz;
        }

        public Sutemeny()
        {
            
        }

        public Sutemeny(string nev, Random rnd)
        {
            this.nev = nev;
            this.liszt = rnd.Next(0, 1001);
            this.tojasDb = rnd.Next(0, 100);
            if (rnd.Next(0, 2) == 0)
                cukormaz = true;
            else
                cukormaz = false;
        }



        // **** METÓDUSOK ****

        // - „nincs” static kulcsszó

        public string Adatok()
        {
            string adatok = $"{nev}, hozzávalók: {liszt} g liszt, {tojasDb} db tojás, cukormáz: {cukormaz}";
            return adatok;
        }

        public void TojasNovel()
        {
            this.tojasDb++;
        }
    }
}
