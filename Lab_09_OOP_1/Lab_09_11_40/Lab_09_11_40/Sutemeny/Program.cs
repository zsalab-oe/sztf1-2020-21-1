﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_OOP_Alapok
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            Sutemeny piskota = new Sutemeny();
            piskota.nev = "Piskóta";
            piskota.liszt = 100;
            piskota.tojasDb = 4;
            //piskota.cukormaz = true;

            Sutemeny dobostorta = new Sutemeny("Dobostorta", 200, 4, true);

            Console.WriteLine(piskota.nev);
            Console.WriteLine(dobostorta.nev);
            string piskotaLeiras = piskota.Adatok();
            Console.WriteLine(piskotaLeiras);
            Console.WriteLine(dobostorta.Adatok());

            Console.ReadLine();
        }
    }
}
