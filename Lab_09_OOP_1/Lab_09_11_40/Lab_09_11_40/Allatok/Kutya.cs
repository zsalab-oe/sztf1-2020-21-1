﻿namespace _2_Allatok
{
    class Kutya
    {
        string nev;
        int labakSzama;
        int farokHossz;
        public bool rosszcsont;

        public Kutya(string nev, int labakSzama, int farokHossz, bool rosszcsont)
        {
            this.nev = nev;
            this.labakSzama = labakSzama;
            this.farokHossz = farokHossz;
            this.rosszcsont = rosszcsont;
        }

        public Kutya()
        {
            
        }

        public Kutya(string nev, bool rosszcsont)
        {
            this.nev = nev;
            this.rosszcsont = rosszcsont;
        }

        public string Udvozol()
        {
            return $"Vaú ({nev})";
        }
    }
}
