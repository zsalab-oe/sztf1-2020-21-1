﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace _2_Allatok
{
    class Macska
    {
        private string nev;
        private int megivottTejMennyiseg;
        private string szin;
        private int eletkor;
        private bool hullikASzore;

        public Macska(string nev, int megivottTejMennyiseg, string szin, int eletkor, bool hullikASzore)
        {
            this.nev = nev;
            this.megivottTejMennyiseg = megivottTejMennyiseg;
            this.szin = szin;
            this.eletkor = eletkor;
            this.hullikASzore = hullikASzore;
        }

        private void Nyavog()
        {
            Console.WriteLine("miau");
        }

        public void TejetIszik(int mennyiseg)
        {
            if (mennyiseg > 10)
            {
                Nyavog();
            }
            megivottTejMennyiseg += mennyiseg;
        }
    }
}
