﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Allatok
{
    class Program
    {
        static void Main(string[] args)
        {
            Macska[] macskak = new Macska[4];
            macskak[0] = new Macska("Cirmi", 0, "szürke", 4, false);
            macskak[1] = new Macska("Kormos", 0, "fekete", 3, false);
            macskak[2] = new Macska("Sahnat", 0, "fekete", 666, true);
            macskak[3] = new Macska("Pocak", 0, "feher", 1, false);

            Itat(macskak);

            int[] ertekek = new int[] { 3, 4, 5, 105 };

            Kutya[] kutyusok = new Kutya[5]
            {
                new Kutya("Buksi", 4, 10, false),
                new Kutya("Zsömle", 2, 15, true),
                new Kutya("Morzsa", 5, 100, false),
                new Kutya("Hercules", 4, 5, true),
                new Kutya("Tiki", 4, 10, false)
            };

            Udvozles(kutyusok);


            Console.ReadLine();

        }

        static void Udvozles(Kutya[] kutyusok)
        {
            for (int i = 0; i < kutyusok.Length; i++)
            {
                if (kutyusok[i].rosszcsont)
                    Console.WriteLine(kutyusok[i].Udvozol());
            }
        }

        static void Itat(Macska[] macskak)
        {
            Random rnd = new Random();
            for (int i = 0; i < macskak.Length; i++)
            {
                macskak[i].TejetIszik(rnd.Next(0, 50));
            }
        }
    }
}
