﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataGenerator;

namespace SZTF1_D_Sziget
{
    class Program
    {
        static void Main(string[] args)
        {
            string adatsor = @"0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*0,0,0,868,741,56,222,186,540,22,0,0,622,230,0,0,0,0,0,0*0,0,0,60,54,731,491,810,269,486,0,0,411,122,0,0,0,0,0,0*0,0,0,736,41,228,56,141,990,424,0,0,596,569,792,0,0,0,0,0*0,893,297,807,822,950,328,122,783,662,308,219,189,451,808,0,0,0,0,0*0,424,607,592,578,915,809,182,627,491,123,489,80,456,763,159,0,0,0,0*0,656,166,64,586,24,367,791,746,381,830,575,195,692,642,798,232,0,0,0*0,852,152,733,282,411,142,144,447,3,231,512,477,206,825,901,224,953,93,0*0,856,692,537,605,671,434,18,715,167,695,262,454,484,261,591,886,197,464,0*0,976,493,604,373,989,55,917,833,101,0,796,362,903,732,871,361,992,195,0*0,0,0,268,72,272,296,240,208,465,767,758,827,579,573,155,755,788,190,0*0,0,0,236,996,677,190,165,717,781,500,546,50,526,759,628,52,552,524,0*0,0,0,192,855,485,880,601,504,614,230,936,813,535,468,72,53,654,874,0*0,0,96,802,609,725,272,744,609,683,555,443,120,87,149,824,563,482,166,0*0,0,698,922,275,721,434,165,354,695,393,942,787,183,357,660,925,419,988,0*0,59,994,420,0,0,438,869,134,196,580,237,771,199,213,875,824,962,374,0*0,992,544,799,0,0,583,727,265,408,635,929,1,7,154,935,81,868,435,0*0,697,666,298,0,0,0,709,424,1,241,879,257,360,542,596,62,232,660,0*0,504,449,378,0,0,0,898,180,979,118,126,869,8,271,899,168,123,467,0*0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
            int[,] sziget = null;

            /* 1. feladat */
            MatrixGenerator(adatsor, ref sziget);

            /* 2. feladat */
            Console.WriteLine($"Átlagmagasság: {Atlagmagassag(sziget)} méter.");
            Console.WriteLine();

            /* 3. feladat */
            Console.WriteLine($"A szigeten {VizekSzama(sziget)} db olyan területi egység van, amelyet víz borít.");
            Console.WriteLine();

            /* 4. feladat */
            Console.WriteLine($"Legmagassabb mért magasság: {LegmagabbPont(sziget)} méter.");
            Console.WriteLine();

            /* 5. feladat */
            Console.Write("Van belső tó? ");
            if (BelsoTo(sziget))
                Console.WriteLine("Van.");
            else
                Console.WriteLine("Nincs.");
            Console.WriteLine();

            /* 6. feladat */
            string[] vizesTeruletek = VizesTeruletek(sziget);
            Console.WriteLine("Vizes területek:");
            for (int i = 0; i < vizesTeruletek.Length; i++)
            {
                Console.WriteLine(">> " + vizesTeruletek[i]);
            }
            Console.WriteLine();

            /* 7. feladat */
            int[] antennaHelye = IdealisAntennaHelye(sziget);
            if (antennaHelye[0] != -1)
                Console.WriteLine($"Az antenna ideális koordinátái: {antennaHelye[0]}, {antennaHelye[1]}.");
            else
                Console.WriteLine("Nincs ilyen.");
            Console.WriteLine();

            /* 8. feladat */
            Console.WriteLine("Stats:");
            string[,] stats = Statisztika(sziget);
            for (int i = 0; i < stats.GetLength(0); i++)
            {
                Console.WriteLine($"{stats[i, 0]}: {stats[i, 1]}");
            }

            ;
        }

        static void MatrixGenerator(string adatsor, ref int[,] sziget)
        {
            string[] sorok = adatsor.Split('*');

            int sorokSzama = sorok.Length;
            int oszlopokSzama = CharDb(sorok[0], ',') + 1;
            sziget = new int[sorokSzama, oszlopokSzama];
            for (int sor = 0; sor < sorokSzama; sor++)
            {
                string[] egySor = sorok[sor].Split(',');
                for (int oszlop = 0; oszlop < egySor.Length; oszlop++)
                {
                    sziget[sor, oszlop] = int.Parse(egySor[oszlop]);
                }
            }
        }

        static int CharDb(string adat, char c)
        {
            int db = 0;
            for (int i = 0; i < adat.Length; i++)
            {
                if (adat[i] == c)
                    db++;
            }
            return db;
        }

        static double Atlagmagassag(int[,] sziget)
        {
            int ossz = 0;
            for (int sor = 0; sor < sziget.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < sziget.GetLength(1); oszlop++)
                {
                    ossz += sziget[sor, oszlop];
                }
            }
            return (double)ossz / sziget.Length; // return (double)ossz / (sziget.GetLength(0) * sziget.GetLength(1));
        }

        static int VizekSzama(int[,] sziget)
        {
            int db = 0;
            for (int sor = 0; sor < sziget.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < sziget.GetLength(1); oszlop++)
                {
                    if (sziget[sor, oszlop] == 0)
                        db++;
                }
            }
            return db;
        }

        static int LegmagabbPont(int[,] sziget)
        {
            int maxSor = 0, maxOszlop = 0;
            int sor = 0, oszlop = 1;
            while (sor < sziget.GetLength(0))
            {
                while (oszlop < sziget.GetLength(1))
                {
                    if (sziget[sor, oszlop] > sziget[maxSor, maxOszlop])
                    {
                        maxSor = sor;
                        maxOszlop = oszlop;
                    }
                    oszlop++;
                }
                sor++;
                oszlop = 0;
            }
            return sziget[maxSor, maxOszlop];
        }

        static bool BelsoTo(int[,] sziget)
        {
            bool van = false;
            int sor = 1;
            int oszlop = 1;
            while (!van && sor < sziget.GetLength(0) - 1)
            {
                while (oszlop < sziget.GetLength(1) - 1 && !BelsoToVizsgalat(sziget, sor, oszlop))
                {
                    oszlop++;
                }
                if (oszlop < sziget.GetLength(1) - 1)
                {
                    van = true;
                }
                else
                {
                    sor++;
                    oszlop = 1;
                }
            }
            return van;
        }

        static bool BelsoToVizsgalat(int[,] sziget, int sor, int oszlop)
        {
            return sziget[sor, oszlop] == 0 &&
                sziget[sor - 1, oszlop] != 0 &&
                sziget[sor + 1, oszlop] != 0 &&
                sziget[sor, oszlop - 1] != 0 &&
                sziget[sor, oszlop + 1] != 0;
        }

        static string[] VizesTeruletek(int[,] sziget)
        {
            int db = VizekSzama(sziget);
            string[] vizesTeruletek = new string[db];
            int k = 0;
            int sor = 0, oszlop = 0;
            while (k < db)
            {
                if (sziget[sor, oszlop] == 0)
                {
                    vizesTeruletek[k] = $"{sor}#{oszlop}";
                    k++;
                }
                oszlop++;
                if (oszlop == sziget.GetLength(1))
                {
                    sor++;
                    oszlop = 0;
                }
            }
            return vizesTeruletek;
        }

        static int[] IdealisAntennaHelye(int[,] sziget)
        {
            int maxErtek = int.MinValue;
            int maxSor = -1, maxOszlop = -1;
            for (int sor = 0; sor < sziget.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < sziget.GetLength(1); oszlop++)
                {
                    if (sziget[sor, oszlop] >= 200 &&
                        sziget[sor, oszlop] <= 400 &&
                        sziget[sor, oszlop] > maxErtek)
                    {
                        maxErtek = sziget[sor, oszlop];
                        maxSor = sor;
                        maxOszlop = oszlop;
                    }
                }
            }
            return new int[] { maxSor, maxOszlop };
        }

        static string[,] Statisztika(int[,] sziget)
        {
            string[,] stats = new string[4, 2];
            stats[0, 0] = "Tenger";
            stats[1, 0] = "Síkság";
            stats[2, 0] = "Dombság";
            stats[3, 0] = "Hegység";

            int[] darabszamok = GetDarabszamok(sziget);
            for (int i = 0; i < darabszamok.Length; i++)
            {
                stats[i, 1] = darabszamok[i].ToString();
            }
            return stats;
        }

        static int[] GetDarabszamok(int[,] sziget)
        {
            int[] darabszamok = new int[4];
            for (int sor = 0; sor < sziget.GetLength(0); sor++)
            {
                for (int oszlop = 0; oszlop < sziget.GetLength(1); oszlop++)
                {
                    if (sziget[sor, oszlop] == 0)
                        darabszamok[0]++;
                    else if (sziget[sor, oszlop] <= 200)
                        darabszamok[1]++;
                    else if (sziget[sor, oszlop] <= 600)
                        darabszamok[2]++;
                    else
                        darabszamok[3]++;
                }
            }
            return darabszamok;
        }
    }
}
