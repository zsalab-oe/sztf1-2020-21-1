﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tester
{
    public class TestClass
    {
        [TestCase(TestName = "MatrixGenerator", Description = "szövegfeldolgozás önállan oké")]
        public static void FeldolgozasTeszt()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("MatrixGenerator", BindingFlags.NonPublic | BindingFlags.Static);
            string egysor = @"32000,6000,43000,83000,4100000,11000,40000,9100000,69000,86000*51000,22000,49000,95000,28000,3000,7000,69000,55000,8000";
            int[,] sziget = null;
            object[] parameters = new object[] { egysor, sziget };
            method.Invoke(null, parameters);
            sziget = (int[,])parameters[1];
            Assert.That(
                sziget.GetLength(0) == 2 && sziget.GetLength(1) == 10 &&
                sziget[0, 0] == 32000 &&
                sziget[0, 1] == 6000 &&
                sziget[1, 0] == 51000 &&
                sziget[1, 1] == 22000
                );
        }

        [TestCase(TestName = "MatrixGenerator", Description = "szövegfeldolgozás automatikusan oké")]
        public static void FeldolgozasTeszt2()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("MatrixGenerator", BindingFlags.NonPublic | BindingFlags.Static);
            string egysor = @"32000,6000,43000,83000,4100000,11000,40000,9100000,69000,86000*51000,22000,49000,95000,28000,3000,7000,69000,55000,8000";
            int[,] sziget = null;
            object[] parameters = new object[] { egysor, sziget };
            method.Invoke(null, parameters);
            sziget = (int[,])parameters[1];
            Assert.That(
                sziget.GetLength(0) == 20 && sziget.GetLength(1) == 20 &&
                sziget[4, 1] == 893 &&
                sziget[4, 2] == 297 &&
                sziget[5, 1] == 424 &&
                sziget[5, 2] == 607
                );
        }

        [TestCase(TestName = "MatrixGenerator", Description = "szövegfeldolgozás manualba jó, de 20x20as hardcodeolva")]
        public static void FeldolgozasTeszt3()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("MatrixGenerator", BindingFlags.NonPublic | BindingFlags.Static);
            string egysor = @"32000-6000-43000-83000-4100000-11000-40000-9100000-69000-86000,51000-22000-49000-95000-28000-3000-7000-69000-55000-8000";
            int[,] sziget = null;
            object[] parameters = new object[] { egysor, sziget };
            method.Invoke(null, parameters);
            sziget = (int[,])parameters[1];

            Assert.That(sziget.Length == 400);
        }

        [TestCase(TestName = "Atlagmagassag", Description = "átlagmagasság egész vagy tört")]
        public static void AvgTest()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("Atlagmagassag", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget= {
            { 1,3,4},
            { 5,6,7},
            { 8,9,10}
            }; //53 //

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            double avg = (double)res;

            Assert.That(avg == 5 || avg == (double)53/9);
        }

        [TestCase(TestName = "Atlagmagassag", Description = "átlagmagasság tört")]
        public static void AVGTest2()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("Atlagmagassag", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,3,4},
            { 5,6,7},
            { 8,9,10}
            }; //53 //

            object[] parameters = new object[] { sziget };
            var res = method.Invoke(null, parameters);
            double avg = (double)res;

            Assert.That(avg == (double)53 / 9);
        }

        [TestCase(TestName = "VizekSzama", Description = "vizek száma oké")]
        public static void CountTest()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("VizekSzama", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4},
            { 0,6,7},
            { 8,0,10}
            }; //3

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            int sum = (int)res;

            Assert.That(sum == 3);
        }

        [TestCase(TestName = "LegmagabbPont", Description = "legmagasabb pont értéke oké")]
        public static void MaxTest()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("LegmagabbPont", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4},
            { 0,10,7},
            { 8,0,5}
            }; //3

            object[] parameters = new object[] { sziget };
            var res = method.Invoke(null, parameters);
            int sum = (int)res;

            Assert.That(sum == 10);
        }

        [TestCase(TestName = "BelsoTo", Description = "belső tó true, ha van benne")]
        public static void BoolTest()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("BelsoTo", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4,5,6,7},
            { 6,3,7,7,6,8},
            { 6,3,0,7,6,8},
            { 6,3,7,7,6,8},
            { 6,3,7,7,6,8}
            }; //3

            object[] parameters = new object[] { sziget };
            var res = method.Invoke(null, parameters);
            bool resc = (bool)res;

            Assert.That(resc == true);
        }

        [TestCase(TestName = "BelsoTo", Description = "belső tó false, ha nincs benne")]
        public static void BoolTest2()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("BelsoTo", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4,5,6,7},
            { 6,3,7,7,6,8},
            { 6,3,8,7,6,8},
            { 6,3,7,7,6,8},
            { 6,3,7,7,6,8}
            }; //3

            object[] parameters = new object[] { sziget };
            var res = method.Invoke(null, parameters);
            bool resc = (bool)res;

            Assert.That(resc == false);
        }

        [TestCase(TestName = "VizesTeruletek", Description = "Kiválogatás oké, lehetnek extra elemek")]
        public static void KivalogatasTeszt()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("VizesTeruletek", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4,5,6,7},
            { 6,3,7,7,6,8},
            { 6,3,8,7,6,8},
            { 0,3,7,7,6,8},
            { 6,3,7,7,6,0}
            }; //3

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            string[] eredm = (string[])res;


            Assert.That(
                   eredm.Contains("0#1") &&
                   eredm.Contains("3#0") &&
                   eredm.Contains("4#5"));
        }

        [TestCase(TestName = "VizesTeruletek", Description = "kiválogatás oké, nincsenek extra elemek")]
        public static void KivalogatasTeszt2()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("VizesTeruletek", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 1,0,4,5,6,7},
            { 6,3,7,7,6,8},
            { 6,3,8,7,6,8},
            { 0,3,7,7,6,8},
            { 6,3,7,7,6,0}
            }; //3

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            string[] eredm = (string[])res;


            Assert.That(
                   eredm.Length == 3 &&
                   eredm.Contains("0#1") &&
                   eredm.Contains("3#0") &&
                   eredm.Contains("4#5"));
        }

        [TestCase(TestName = "IdealisAntennaHelye", Description = "kiv_maxkiválasztás oké")]
        public static void PMaxTeszt()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("IdealisAntennaHelye", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget= {
            { 300,50,410},
            { 220,700,390},
            { 999,180,540}
            };

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            int[] eredm = (int[])res;


            Assert.That(
                   eredm[0] == 1 && eredm[1] == 2);
        }

        [TestCase(TestName = "IdealisAntennaHelye", Description = "első elemre is figyelt")]
        public static void PMaxTeszt2()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("IdealisAntennaHelye", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 9999,50,410},
            { 220,700,390},
            { 999,180,540}
            };

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            int[] eredm = (int[])res;


            Assert.That(
                   eredm[0] == 1 && eredm[1] == 2);
        }

        [TestCase(TestName = "IdealisAntennaHelye", Description = "lekezelte, ha nincs ilyen elem")]
        public static void PMaxTeszt3()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("IdealisAntennaHelye", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget = {
            { 9999,5000,4100},
            { 2200,7000,3900},
            { 9990,1800,5400}
            };

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            int[] eredm = (int[])res;


            Assert.That(
                   eredm[0] == -1 && eredm[1] == -1);
        }


        [TestCase(TestName = "Statisztika", Description = "rendben van")]
        public static void StatTest1()
        {
            Type t = Assembly.Load("SZTF1_D_Sziget").GetTypes().FirstOrDefault(z => z.Name.Contains("Program"));
            var method = t.GetMethod("Statisztika", BindingFlags.NonPublic | BindingFlags.Static);

            int[,] sziget= {
            { 10,20,30},
            { 300,410,550},
            { 850,860,870},
            { 0,0,300}
            };

            object[] parameters = new object[] { sziget};
            var res = method.Invoke(null, parameters);
            string[,] eredm = (string[,])res;



            Assert.That(
                   eredm[0, 0].ToUpper() == "TENGER" &&
                   eredm[1, 0].ToUpper() == "SÍKSÁG" &&
                   eredm[2, 0].ToUpper() == "DOMBSÁG" &&
                   eredm[3, 0].ToUpper() == "HEGYSÉG" &&
                   eredm[0, 1] == "2" &&
                   eredm[1, 1] == "3" &&
                   eredm[2, 1] == "4" &&
                   eredm[3, 1] == "3");
        }

    }
}