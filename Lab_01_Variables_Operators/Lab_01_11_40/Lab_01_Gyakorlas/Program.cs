﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01_Gyakorlas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ez az első programom!");

            int egeszSzam;                  // 1. deklarálás
            egeszSzam = 5;                  // 2. értékadás
            int masikSzam = 2 * egeszSzam;  // 3. érték lekérdezése
            Console.WriteLine(egeszSzam);   // 3. érték lekérdezése

            bool logikai = true;            // inicializálás (deklarálás + értékadás)

            int zsebpenz = 500;

            double tortszam = 4.56d;
            // tizedesponttal megadott literálok (értékek) alapértelmezetten double típusúak
            // számliterál megadása mindig tizedesPONTTAL!
            // string -> lebegőpontos konverzió esetén a windows lokalizációból szedi, hogy hogyan kell szövegesen tárolni a tizedes-elválasztót, ezt keresi a stringben (alapértelmezetten VESSZŐ)




            /* ======== Kasztolás ======== */

            // Kasztolni kell: ha értékvesztés TÖRTÉNHET
            //  - kisebb értelmezési tartományba történő konvertálásnál („történhet”, vagyis mindegy, hogy az aktuális érték pont beleférne – a típus a lényeg)
            //  - lebegőpontosból egésszé történő konvertálásnál
            //  - előjelesség szerinti típusváltásnál

            // Típuskényszerítés, "kasztolás" (ebben a félévben számok között):
            // célváltozó = (céltípus)forrásváltozó;


            int egesz = (int)tortszam;

            char c = 'A';
            int szam = c;

            Console.WriteLine(szam);

            decimal penz = 1234.567m;
            decimal masikPenz = (decimal)13213546.7895;



            /* konverzió: bármilyen típus -> string */

            // Stringgé konvertálásnál:
            //célváltozó = forrásváltozó.ToString();
            string szoveg = "ez egy szöveg";
            string szovegesen = tortszam.ToString();
            Console.WriteLine(szovegesen);
            Console.WriteLine(tortszam);



            /* konverzió: string -> szám */

            // Stringből konvertálásnál:
            //célváltozó = céltípus.Parse(stringváltozó);
            szovegesen = "5,78";
            float szamkent = float.Parse(szovegesen);
            szamkent = szamkent * 2;
            Console.WriteLine(szamkent);


            int x = 5;
            int y = 3;
            int eredmeny = x / y;
            Console.WriteLine(eredmeny);
            int maradek = x % y;
            Console.WriteLine(maradek);
            double pontosEredmeny = (double)x / y;
            Console.WriteLine(pontosEredmeny);

            x = 5;
            eredmeny = x++ + ++x + x++;
            //       = 5   + 7   + 7

            Console.WriteLine(x);           // x = 8
            Console.WriteLine(eredmeny);    // eredmeny = 19


            //x = 5;
            //y = 0;
            //eredmeny = x / y;
            //Console.WriteLine(eredmeny);

            double a = 5;
            double b = 0;
            double eredmenyPontos = a / b;
            Console.WriteLine(eredmenyPontos);

            // Pizza
            Console.Write("32cm-es pizza ára: ");
            string ar32Szovegkent = Console.ReadLine();
            int ar32 = int.Parse(ar32Szovegkent);
            Console.Write("45cm-es pizza ára: ");
            int ar45 = int.Parse(Console.ReadLine());
            
            double sugar32 = 16;
            double sugar45 = 22.5;
            const double PI = 3.14159;

            double terulet32 = sugar32 * sugar32 * PI;
            double terulet45 = sugar45 * sugar45 * PI;
            
            double egysegar32 = ar32 / terulet32;
            double egysegar45 = ar45 / terulet45;
            
            Console.WriteLine(egysegar32);
            Console.WriteLine(egysegar45);



            // string összefűzés
            string egyik = "ingyenkonyha";
            string masik = "lány";
            string osszefuzve = egyik + masik;
            Console.WriteLine(osszefuzve);

            Console.WriteLine("szöveg eleje" + " szöveg közepe, változó értéke:" + terulet32 + ", szöveg vége");


            Console.Write("Add meg a neved: ");
            string nev = Console.ReadLine();
            Console.Write("Add meg a bruttó fizetésed: ");
            decimal brutto = decimal.Parse(Console.ReadLine());
            decimal netto = brutto * 0.66m;
            Console.WriteLine("Kedves " + nev + ", ennyi a nettó fizetésed: " + netto + " Ft");










            Console.ReadLine();
        }
    }
}
