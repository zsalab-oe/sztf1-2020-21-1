﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_01_gyak
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello C# világ!");


            int szam;                       // 1. deklarálás
            szam = 10;                      // 2. értékadás
            Console.WriteLine(szam);        // 3. érték lekérdezése
            int masikSzam = 2 * szam;       // 3. érték lekérdezése
            Console.WriteLine(masikSzam);   // érték lekérdezése

            string szoveg = "egy szöveg";   // inicializáció (deklarálás + értékadás)

            double tortszam = szam;

            tortszam = 4.56;
            // tizedesponttal megadott literálok (értékek) alapértelmezetten double típusúak
            // számliterál megadása mindig tizedesPONTTAL!
            // string -> lebegőpontos konverzió esetén a windows lokalizációból szedi, hogy hogyan kell szövegesen tárolni a tizedes-elválasztót, ezt keresi a stringben (alapértelmezetten VESSZŐ)



            /* ======== Kasztolás ======== */

            // Kasztolni kell: ha értékvesztés TÖRTÉNHET
            //  - kisebb értelmezési tartományba történő konvertálásnál („történhet”, vagyis mindegy, hogy az aktuális érték pont beleférne – a típus a lényeg)
            //  - lebegőpontosból egésszé történő konvertálásnál
            //  - előjelesség szerinti típusváltásnál

            // Típuskényszerítés, "kasztolás" (ebben a félévben számok között):
            // célváltozó = (céltípus)forrásváltozó;

            int a = (int)tortszam;
            Console.WriteLine(a);

            decimal nagyonPontos = (decimal)tortszam;



            /* konverzió: bármilyen típus -> string */

            // Stringgé konvertálásnál:
            //célváltozó = forrásváltozó.ToString();
            string szovegesen = tortszam.ToString();
            Console.WriteLine(szovegesen);



            /* konverzió: string -> szám */

            // Stringből konvertálásnál:
            //célváltozó = céltípus.Parse(stringváltozó);
            tortszam = double.Parse(szovegesen);
            tortszam = double.Parse("13,4");




            // 0-val osztás
            int x = 5;
            int y = 0;
            //int eredmeny = x / y;
            //Console.WriteLine(eredmeny);
            //int maradek = x % y;
            //Console.WriteLine(maradek);
            double pontosEredmeny = (double)x / y;
            Console.WriteLine(pontosEredmeny);

            x = 5;
            int eredmeny = x++ + ++x + x++;
            //             5   +  7  + 7
            Console.WriteLine(x);           // x = 8
            Console.WriteLine(eredmeny);    // eredmeny = 19



            // string összefűzés
            Console.Write("Adj meg egy szöveget: ");
            string megadottSzoveg = Console.ReadLine();
            Console.WriteLine("A beírt szöveg: " + megadottSzoveg);

            string egyik = "ingyenkonyha";
            string masik = "lány";
            string osszefuzve = egyik + masik;
            Console.WriteLine(osszefuzve);


            Console.Write("Add meg a neved: ");
            string nev = Console.ReadLine();
            Console.WriteLine("Szia, " + nev + "!");




            Console.Write("Add meg a mostani hőmérsékletet (Celsius fokban): ");
            double C = double.Parse(Console.ReadLine());
            double F = C * 9 / 5 + 32;
            Console.WriteLine("Hőmérséklet: " + F + "°F");


            double tort = 0.66d;

            Console.Write("Add meg a neved: ");
            string felhNev = Console.ReadLine();
            Console.Write("Add meg a bruttó fizetésed: ");
            decimal brutto = decimal.Parse(Console.ReadLine());
            decimal netto = brutto * 0.66m;
            string megjelenitendoSzoveg = "Kedves " + felhNev + ", nettó fizetésed: " + netto + "Ft";
            Console.WriteLine(megjelenitendoSzoveg);






            // Pizza
            Console.Write("32cm-es pizza ára: ");
            string ar32Szovegkent = Console.ReadLine();
            int ar32 = int.Parse(ar32Szovegkent);
            Console.Write("45cm-es pizza ára: ");
            int ar45 = int.Parse(Console.ReadLine());

            double sugar32 = 16;
            double sugar45 = 22.5;
            const double PI = 3.14159;

            double terulet32 = sugar32 * sugar32 * PI;
            double terulet45 = sugar45 * sugar45 * PI;

            double egysegar32 = ar32 / terulet32;
            double egysegar45 = ar45 / terulet45;

            Console.WriteLine(egysegar32);
            Console.WriteLine(egysegar45);







            Console.ReadLine();
        }
    }
}
