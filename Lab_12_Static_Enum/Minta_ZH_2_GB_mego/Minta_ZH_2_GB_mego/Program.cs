﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minta_ZH_2_GB_mego
{
    class Program
    {
        static void Main(string[] args)
        {
            Flotta a = new Flotta(Nemzet.Brit);
            Flotta b = new Flotta(Nemzet.Holland);
            a.Ellenseg = b;
            b.Ellenseg = a;

            Csata(a, b);

            Console.ReadLine();
        }

        static void Csata(Flotta a, Flotta b)
        {
            Console.WriteLine(a.Allapot());
            Console.WriteLine(b.Allapot());
            Console.WriteLine();
            bool aTamad = true;
            do
            {
                if (aTamad)
                {
                    a.Tamad();
                }
                else
                {
                    b.Tamad();
                }
                Console.WriteLine(a.Allapot());
                Console.WriteLine(b.Allapot());
                Console.WriteLine();
                aTamad = !aTamad;
            } while (!a.VanElsullyedt() && !b.VanElsullyedt());
            Kiir(a, b);
        }

        static void Kiir(Flotta a, Flotta b)
        {
            Flotta vesztes = a.VanElsullyedt() ? a : b;
            string s = "vesztes: " + vesztes.Zaszlo.ToString();
            Console.WriteLine(s);
            System.IO.File.WriteAllText("csata.txt", s);
        }
    }
}
