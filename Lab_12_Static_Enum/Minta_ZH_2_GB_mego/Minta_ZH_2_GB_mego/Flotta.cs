﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minta_ZH_2_GB_mego
{
    enum Nemzet { Brit, Holland }

    class Flotta
    {
        static Random rnd = new Random();

        Nemzet zaszlo;
        Hajo[] hajohad;
        Flotta ellenseg;
        int tamadoHajoIndex;
        public Nemzet Zaszlo { get { return zaszlo; } }
        public Flotta Ellenseg { set { ellenseg = value; } }

        public Flotta(Nemzet zaszlo)
        {
            this.zaszlo = zaszlo;
            this.hajohad = new Hajo[5];
            for (int i = 0; i < hajohad.Length; i++)
            {
                hajohad[i] = new Hajo(20);
            }
            tamadoHajoIndex = 0;
        }

        public void Tamad()
        {
            Hajo tamado = hajohad[tamadoHajoIndex % hajohad.Length];
            Hajo celpont = ellenseg.hajohad[rnd.Next(0, ellenseg.hajohad.Length)];
            tamado.Lo(celpont);
            tamadoHajoIndex++;
        }

        public bool VanElsullyedt()
        {
            int j = 0;
            while (j < hajohad.Length && !hajohad[j].Elsullyedt)
                j++;
            return j < hajohad.Length;
        }

        int OsszLegenyseg()
        {
            int ossz = 0;
            foreach (Hajo h in hajohad)
                ossz += h.Legenyseg;
            return ossz;
        }

        public string Allapot()
        {
            //return AllapotRovid();
            return AllapotBovitett();
        }

        string AllapotRovid()
        {
            return $"{zaszlo.ToString()}, legénység: {OsszLegenyseg()}";
        }

        string AllapotBovitett()
        {
            string allapot = AllapotRovid() + " (";
            foreach (Hajo h in hajohad)
                allapot += h.Legenyseg + ", ";
            allapot = allapot.TrimEnd(' ', ',');
            allapot += ")";
            return allapot;
        }
    }
}
