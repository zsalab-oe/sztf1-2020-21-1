﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minta_ZH_2_GB_mego
{
    class Hajo
    {
        static Random rnd = new Random();

        int legenyseg;
        public int Legenyseg { get { return legenyseg; } }
        public bool Elsullyedt { get { return legenyseg == 0; } }

        public Hajo(int legenyseg)
        {
            this.legenyseg = legenyseg;
        }

        void TalalatotKap()
        {
            legenyseg--;
        }

        public void Lo(Hajo celpont)
        {
            if (rnd.Next(0, 10) != 0)
                celpont.TalalatotKap();
        }
    }
}
