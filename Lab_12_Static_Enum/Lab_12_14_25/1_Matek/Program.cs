﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Matek
{
    class Matek
    {
        public double Kerulet(double sugar)
        {
            return 2 * sugar * Math.PI;
        }
    }

    class MatekS
    {
        public static double Kerulet(double sugar)
        {
            return 2 * sugar * Math.PI;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Matek m = new Matek();
            Console.WriteLine(m.Kerulet(5));

            // statikus adattag elérése az osztályon keresztül lehetséges
            //OsztályNeve.Tulajdonság vagy OsztályNeve.Metódus()
            Console.WriteLine(MatekS.Kerulet(5));
        }
    }
}
