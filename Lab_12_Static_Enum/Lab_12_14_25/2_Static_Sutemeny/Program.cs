﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Static_Sutemeny
{
    class Program
    {
        static void Main(string[] args)
        {
            // statikus adattag elérése az osztályon keresztül lehetséges
            //OsztályNeve.Tulajdonság vagy OsztályNeve.Metódus()
            Console.WriteLine(Sutemeny.PeldanyDb);

            // elérhetetlen memóriaterületek generálása azzal, hogy létrehozzuk az objektumokat, de egyből megszüntetjük a rájuk mutató hivatkozásokat (maguk az objektumok ilyenkor még bent maradnak a memóriában)
            for (int i = 0; i < 10; i++)
            {
                Sutemeny s = new Sutemeny();
            }

            // memóriafelszabadítás "kikényszerítése" Garbage Collector (szemétgyűjtő) segítségével
            GC.Collect();

            Console.WriteLine("Ez vajon a szemétgyűjtés után fut le?");

            Console.WriteLine(Sutemeny.PeldanyDb);

            Console.ReadLine();
        }
    }
}
