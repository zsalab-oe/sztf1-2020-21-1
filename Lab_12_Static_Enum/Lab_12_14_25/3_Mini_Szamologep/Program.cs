﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Mini_Szamologep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("a: ");
            double a = double.Parse(Console.ReadLine());
            Console.Write("b: ");
            double b = double.Parse(Console.ReadLine());
            
            Console.Write("művelet: ");
            ConsoleKeyInfo muvelet = Console.ReadKey();

            Console.Write("\nEredmény: ");
            switch (muvelet.KeyChar)
            {
                case '+': Console.WriteLine(a + b); break;
                case '-': Console.WriteLine(a - b); break;
                case '*': Console.WriteLine(a * b); break;
                case '/': Console.WriteLine(a / b); break;
                default: Console.WriteLine("Nem értelmezhető műveletjel."); break;
            }

            Console.ReadLine();
        }
    }
}
