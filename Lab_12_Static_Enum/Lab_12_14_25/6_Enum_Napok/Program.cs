﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6_Enum_Napok
{
    enum Nap
    {
        Hetfo, Kedd, Szerda, Csutortok, Pentek, Szombat, Vasarnap
    }

    class Program
    {
        static void Main(string[] args)
        {
            Nap meccsnap = Nap.Szerda;
            meccsnap = Nap.Csutortok;

            Nap ma = Nap.Hetfo;

            // konverzió: felsorolástípus -> egész szám
            int szam = (int)ma;
            Console.WriteLine(szam);

            // konverzió: egész szám -> felsorolástípus
            Nap holnap = (Nap)(szam + 1);
            Nap milyenNap = (Nap)1;

            // konverzió: felsorolástípus -> string
            string milyenNapSzovegesen = milyenNap.ToString();
            Console.WriteLine(milyenNapSzovegesen);

            // konverzió: string -> felsorolástípus
            Nap megbeszeles = (Nap)Enum.Parse(typeof(Nap), "Csutortok");

            Console.WriteLine("Megbeszélés: " + megbeszeles.ToString());

            megbeszeles++;

            Console.WriteLine("Megbeszélés új időpontja: " + megbeszeles.ToString());
        }
    }
}
