﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Minta_ZH_Hajok
{
    class Hajo
    {
        int legenyseg;

        public int Legenyseg { get { return legenyseg; } }
        public bool Elsullyedt
        { get { return legenyseg == 0; } }

        public Hajo(int legenyseg)
        {
            this.legenyseg = legenyseg;
        }

        void TalalatotKap()
        {
            this.legenyseg--;
        }

        public void Lo(Hajo celpont)
        {
            if (Seged.Rnd.Next(0, 10) < 9)
            {
                //celpont.legenyseg--;
                celpont.TalalatotKap();
            }
        }
    }
}
