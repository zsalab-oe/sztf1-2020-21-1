﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Minta_ZH_Hajok
{
    enum Nemzet { brit, holland }

    class Flotta
    {
        Nemzet zaszlo;
        Hajo[] hajohad;
        int tamadoHajoIndex;

        public Flotta(Nemzet zaszlo, int hajokSzama)
        {
            this.zaszlo = zaszlo;
            hajohad = new Hajo[hajokSzama];
            for (int i = 0; i < hajohad.Length; i++)
            {
                hajohad[i] = new Hajo(20);
            }
            this.tamadoHajoIndex = 0;
        }

        bool Tamad(Flotta ellenseg)
        {
            Hajo celpont = ellenseg.hajohad[Seged.Rnd.Next(0, ellenseg.hajohad.Length)];
            hajohad[tamadoHajoIndex].Lo(celpont);
            tamadoHajoIndex++;
            if (tamadoHajoIndex == hajohad.Length)
                tamadoHajoIndex = 0;

            // célváltozó = feltétel ? értékHaIgaz : értékHaHamis;
            tamadoHajoIndex =
                tamadoHajoIndex == hajohad.Length - 1 ? 0 : tamadoHajoIndex + 1;

            return celpont.Elsullyedt;
        }

        public bool VanElsullyedt()
        {
            int j = 0;
            while (j < hajohad.Length &&
                !hajohad[j].Elsullyedt)
            {
                j++;
            }
            return j < hajohad.Length;
        }


        string Allapot()
        {
            int osszLeg = OsszLegenyseg();
            string s = $"{zaszlo.ToString()} ({osszLeg})";
            return s;
        }

        int OsszLegenyseg()
        {
            int osszLeg = 0;
            foreach (Hajo h in hajohad)
            {
                osszLeg += h.Legenyseg;
            }
            return osszLeg;
        }
    }
}
