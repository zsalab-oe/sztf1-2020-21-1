﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_Tablan_Mozgas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Tabla";
            int x = 0;
            int y = 0;
            int tablaMeret = 10;
            bool kilep = false;
            do
            {
                Console.Clear();
                Console.WriteLine(TablaRajzol(tablaMeret, x, y));
                ConsoleKeyInfo k = Console.ReadKey();
                switch (k.Key)
                {
                    case ConsoleKey.UpArrow: if (y > 0) y--; break;
                    case ConsoleKey.DownArrow: if (y < tablaMeret - 1) y++; break;
                    case ConsoleKey.LeftArrow: if (x > 0) x--; break;
                    case ConsoleKey.RightArrow: if (x < tablaMeret - 1) x++; break;
                    case ConsoleKey.Escape: kilep = true; break;
                }
            } while (!kilep);
        }

        static string TablaRajzol(int meret, int x, int y)
        {
            string s = "";
            for (int i = 0; i < meret; i++)
            {
                for (int j = 0; j < meret; j++)
                {
                    if (i == y && j == x)
                        s += "*";
                    else
                        s += ".";
                }
                s += "\r\n";
            }
            return s;
        }
    }
}
