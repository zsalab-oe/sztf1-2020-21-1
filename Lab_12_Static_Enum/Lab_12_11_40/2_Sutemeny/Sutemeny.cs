﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Sutemeny
{
    class Sutemeny
    {
        // osztályszintű (statikus) adattagok
        static int peldanyDb;
        static Random rnd;

        // osztályszintű tulajdonság
        public static int PeldanyDb { get { return peldanyDb; } }

        // osztályszintű konstruktor
        // nincs láthatósági jelző (access modifiers are not allowed on static constructors)
        static Sutemeny()
        {
            rnd = new Random();
            peldanyDb = 0;
        }


        // példányszintű adattag
        int ar;

        // konstruktor
        public Sutemeny()
        {
            this.ar = rnd.Next(190, 9991);
            peldanyDb++;
            Console.WriteLine($"{ar} Ft-os süti elkészítve, összesen: {peldanyDb}");
        }

        // "destruktor"
        ~Sutemeny() // AltGr + 1: ~
        {
            peldanyDb--;
            Console.WriteLine($"Egy sütit megettem, ennyi maradt: {peldanyDb}");
        }
    }
}
