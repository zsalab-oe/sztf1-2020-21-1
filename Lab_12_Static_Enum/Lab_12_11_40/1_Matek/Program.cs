﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Matek
{
    class Matek
    {
        // may I have a large container of coffee :)
        //  3. 1   4  1   5       9     2    6
        double PI = 3.1415926;

        public double Add(double a, double b)
        {
            return a + b;
        }
    }

    class MatekS
    {
        static double PI = 3.1415926;

        public static double Add(double a, double b)
        {
            return a + b;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            double x = 3;
            double y = 2.5;

            Matek m = new Matek();
            Console.WriteLine(m.Add(x, y));

            // statikus adattag elérése az osztályon keresztül lehetséges
            //OsztályNeve.Tulajdonság vagy OsztályNeve.Metódus()
            Console.WriteLine(MatekS.Add(x, y));
        }
    }
}
