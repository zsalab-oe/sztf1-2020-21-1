﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Minta_ZH_Hajo
{
    class Hajo
    {
        static Random rnd;

        static Hajo()
        {
            rnd = new Random();
        }

        int legenyseg;

        public bool Elsullyedt
        { get { return legenyseg == 0; } }

        public Hajo(int legenyseg = 20)
        {
            this.legenyseg = legenyseg;
        }

        void TalalatotKap()
        {
            legenyseg--;
        }

        public void Lo(Hajo celpont)
        {
            if (rnd.Next(0, 10) < 9)
                celpont.TalalatotKap();
        }
    }
}
