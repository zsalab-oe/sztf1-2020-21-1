﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_Minta_ZH_Hajo
{
    class Csata
    {
        Flotta flottaA, FlottaB;

        public Csata(Flotta flottaA, Flotta flottaB)
        {
            this.flottaA = flottaA;
            FlottaB = flottaB;
        }

        public void Szimulal()
        {
            do
            {
                flottaA.Tamad(FlottaB);
                FlottaB.Tamad(flottaA);
            } while (!flottaA.VanElsullyedt() || !FlottaB.VanElsullyedt());
        }
    }
}
