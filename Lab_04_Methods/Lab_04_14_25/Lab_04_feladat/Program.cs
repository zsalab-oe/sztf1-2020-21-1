﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_feladat
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tomb = new int[int.Parse(Console.ReadLine())];
            Random rnd = new Random();
            for (int i = 0; i < tomb.Length; i++)
            {
                tomb[i] = rnd.Next(1, 101);
            }

            // maximum-, minimumkiválasztás tétel
            int minIndex = 0;
            for (int i = 1; i < tomb.Length; i++)
            {
                if (tomb[i] < tomb[minIndex])
                    minIndex = i;
            }
            Console.WriteLine(minIndex);
        }
    }
}
