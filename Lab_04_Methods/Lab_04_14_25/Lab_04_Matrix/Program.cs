﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Többdimenziós tömbök */

            // deklaráció:
            // típus[vesszők] tömbnév; (a szögletes zárójelbe dimenziószám-1 darab vesszőt kell tenni)
            // tömblétrehozás:
            // tömbnév = new típus[elemszám1,...,elemszámN]; (az egyes dimenziók elemszámait vesszőkkel elválasztva kell megadni)
            // A deklaráció és a tömblétrehozás itt is összevonható:
            int[,] matrix = new int[4, 7];
            
            Console.WriteLine("Sorok száma: " + matrix.GetLength(0));
            Console.WriteLine("Oszlopok száma: " + matrix.GetLength(1));
            //Console.WriteLine("Cellák száma: " + matrix.Length);

            MatrixFeltolt(matrix);
            Console.WriteLine(TombSzovegkent(matrix));
            Console.WriteLine("Utolsó oszlop összege: " + OszlopOsszeg(matrix, matrix.GetLength(1) - 1));

            Console.ReadLine();
        }

        static void MatrixFeltolt(int[,] m)
        {
            Random rnd = new Random();
            for (int sorIndex = 0; sorIndex < m.GetLength(0); sorIndex++)
            {
                for (int oszlopIndex = 0; oszlopIndex < m.GetLength(1); oszlopIndex++)
                {
                    m[sorIndex, oszlopIndex] = rnd.Next(1, 21);
                }
            }
        }

        //static int[,] MatrixLetrehozas(int m, int n)
        //{
        //    // TODO
        //}

        static string TombSzovegkent(int[,] m)
        {
            string s = "";
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    s += m[i, j] + "\t"; // \t jelentése: tabulátor
                }
                s += "\n"; // \n jelentése: sortörés
            }
            return s;
        }

        static int OszlopOsszeg(int[,] m, int oszlopIndex)
        {
            int osszeg = 0;
            for (int i = 0; i < m.GetLength(0); i++)
            {
                osszeg += m[i, oszlopIndex];
            }
            return osszeg;
        }
    }
}
