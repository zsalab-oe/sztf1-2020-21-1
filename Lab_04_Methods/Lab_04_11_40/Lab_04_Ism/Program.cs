﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_Ism
{
    class Program
    {
        static void Main(string[] args)
        {
            int szam1 = 5;
            int szam2 = 10;
            Osszeado(szam1, szam2);
            Console.WriteLine("szam1 = " + szam1);
            Console.WriteLine("szam2 = " + szam2);

            // címszerinti paraméterátadás
            //   - egy memóriacím kerül átadásra, esetünkben a szam1 és szam2 változók memóriacímei
            //   - jelölés: ref kulcsszóval, a címszerint bekért / átadni kívánt paraméter(ek) előtt
            //   - jelölni kell a metódus fejlécében ÉS a metódus hívásánál is

            OsszeadoRef(ref szam1, ref szam2);
            Console.WriteLine("szam1 = " + szam1);
            Console.WriteLine("szam2 = " + szam2);

            int[] ertekek = new int[10];
            ertekek[0] = 34;
            ertekek[1] = 20;
            ertekek[4] = -7;
            ertekek[6] = 333;
            Console.WriteLine(TombSzovegkent(ertekek));
            TombModosito(ref ertekek);
            Console.WriteLine(TombSzovegkent(ertekek));

            Console.ReadLine();
        }

        static string TombSzovegkent(int[] tomb)
        {
            string s = "";
            for (int i = 0; i < tomb.Length; i++)
            {
                s += tomb[i] + "\t";
            }
            return s;
        }

        static void TombModosito(ref int[] tomb)
        {
            Random rnd = new Random();

            tomb = new int[5];
            for (int i = 0; i < tomb.Length; i++)
            {
                tomb[i] = rnd.Next(0, 50);
            }
        }

        static void Osszeado(int a, int b)
        {
            a = a * a;
            b = b * b;
            Console.WriteLine(a + b);
        }

        static void OsszeadoRef(ref int a, ref int b)
        {
            a = a * a;
            b = b * b;
            Console.WriteLine(a + b);
        }
    }
}
