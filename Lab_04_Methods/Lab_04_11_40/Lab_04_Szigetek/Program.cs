﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_04_Szigetek
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 1. feladat */
            int[] meresiEredmenyek = AdatokGeneralasa(10); /* new[] { 0, 4, 0, 5, 1, 8, 0, 9, 0, 9, 2 }; */

            /* 2. feladat */
            Console.WriteLine(TombSzovegkent(meresiEredmenyek));

            /* 3. feladat */
            int legmagasabbPontHelye = LegmagasabbPontHelye(meresiEredmenyek);
            Console.WriteLine("A legmagasabb pont helye: " + legmagasabbPontHelye);
            int maxMagassag = meresiEredmenyek[legmagasabbPontHelye];
            Console.WriteLine("A magasság itt: " + maxMagassag);

            /* 4. feladat */
            Console.WriteLine("Előfordulások száma: " + ElofordulasokSzama(meresiEredmenyek, maxMagassag));

            /* 5. feladat */
            int maxHossz = LeghosszabbSzakaszHossza(meresiEredmenyek);
            Console.WriteLine("A leghosszabb szigetszakasz hossza: " + maxHossz);

            /* 6. feladat */
            Console.WriteLine("A legmagasabb pont ezen a szakaszon van? " + LegmagasabbPontALeghosszabbSzakaszon(meresiEredmenyek, legmagasabbPontHelye, maxHossz));

            Console.ReadLine();
        }
        static int[] AdatokGeneralasa(int n)
        {
            int[] adatok = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < adatok.Length; i++)
            {
                if (rnd.Next(0, 10) < 4)
                    adatok[i] = rnd.Next(1, 10);
                else
                    adatok[i] = 0;
            }
            return adatok;
        }
        static string TombSzovegkent(int[] adatok)
        {
            string s = "";
            for (int i = 0; i < adatok.Length; i++)
            {
                s += adatok[i] + "\t";
            }
            return s;
        }
        static int LegmagasabbPontHelye(int[] adatok)
        {
            int maxIndex = 0;
            for (int i = 1; i < adatok.Length; i++)
            {
                if (adatok[i] > adatok[maxIndex])
                    maxIndex = i;
            }
            return maxIndex;
        }
        static int ElofordulasokSzama(int[] adatok, int keresett)
        {
            int db = 0;
            for (int i = 0; i < adatok.Length; i++)
            {
                if (adatok[i] == keresett)
                    db++;
            }
            return db;
        }

        static int LeghosszabbSzakaszHossza(int[] adatok)
        {
            int hossz = 0;
            int maxHossz = 0;
            for (int i = 0; i < adatok.Length; i++)
            {
                if (adatok[i] > 0)
                {
                    hossz++;
                }
                else
                {
                    if (hossz > maxHossz)
                        maxHossz = hossz;
                    hossz = 0;
                }
            }
            if (hossz > maxHossz)
                maxHossz = hossz;

            return maxHossz;
        }

        static bool LegmagasabbPontALeghosszabbSzakaszon(int[] adatok, int maxIndex, int maxHossz)
        {
            int hossz = 1;
            int j = maxIndex - 1;
            while (j >= 0 && adatok[j] > 0)
            {
                hossz++;
                j--;
            }
            j = maxIndex + 1;
            while (j < adatok.Length && adatok[j] > 0)
            {
                hossz++;
                j++;
            }
            bool rajtaVan = hossz == maxHossz;
            return rajtaVan;
        }
    }
}
