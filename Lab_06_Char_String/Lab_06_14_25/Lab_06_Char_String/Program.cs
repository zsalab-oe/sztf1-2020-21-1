﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_Char_String
{
    class Program
    {
        static void Main(string[] args)
        {
            char c = 'a';
            char[] teszt = new char[] { 'v', 'a', 'l', 'a', 'm', 'i' };
            teszt[0] = 'V';

            string teszt2 = null;
            teszt2 = "valami"; // immutable type
            //teszt2[0] = 'V';
            teszt2 = teszt2 + "valamimás";
            Console.WriteLine(">> utolsó karakter: " + teszt2[teszt2.Length - 1]);

            /* Fel #1 Magánhangzók */
            Console.WriteLine();
            string szoveg = "Meddig lehet a medvével nyugodtan beszélgetni? Míg félbe nem szakít.";
            Console.WriteLine(szoveg);
            int mghDb = MaganhangzokSzama(szoveg);
            Console.WriteLine($">> A szövegben {mghDb} db magánhangzó van.");

            /* Fel #2 Tevetuvudsz */
            Console.WriteLine();
            string szoveg2 = "TE tUdsz Így beszélni";
            Console.WriteLine(szoveg2);
            Console.WriteLine(">> " + Tevetuvudsz(szoveg2));

            /* Fel #3 Palindrom */
            Console.WriteLine();
            string szoveg3 = "Indul a görög aludni.";
            Console.WriteLine(szoveg3);
            Console.WriteLine(">> Palindrom szöveg-e? " + Palindrom(szoveg3));

            Console.WriteLine();
            PadLeftTeszt();

            /* Fel #4 Kozépre */
            Console.WriteLine();
            string vers = @"Hear the sound of the falling rain
Coming down like an Armageddon flame (hey!)
A shame
The ones who died without a name";
            Kozepre(vers);

            /* Fel #5 Csere */
            Console.WriteLine();
            string szoveg5 = "Amelyik kutya ugat, az a kutya nem harap.";
            Console.WriteLine(szoveg5);
            szoveg5 = szoveg5.Replace("kutya", "macska");
            Console.WriteLine("csere:");
            Console.WriteLine(szoveg5);

            Console.ReadLine();
        }

        

        static bool Maganhangzo(char c)
        {
            string maganhangzok = "aáeéiíoóöőuúüű";
            c = char.ToLower(c);
            //int j = 0;
            //while (j < maganhangzok.Length && maganhangzok[j] != c)
            //{
            //    j++;
            //}
            //return j < maganhangzok.Length;
            return maganhangzok.Contains(c);
        }

        static int MaganhangzokSzama(string szoveg)
        {
            int db = 0;
            for (int i = 0; i < szoveg.Length; i++)
            {
                if (Maganhangzo(szoveg[i]))
                    db++;
            }
            return db;
        }

        // TE tUdsz Így beszélni >> TEvE tUvUdsz ÍvÍgy beveszévélnivi

        static string Tevetuvudsz(string szoveg)
        {
            string atalakitott = "";
            for (int i = 0; i < szoveg.Length; i++)
            {
                atalakitott += szoveg[i];
                if (Maganhangzo(szoveg[i]))
                    atalakitott += "v" + szoveg[i];
            }
            return atalakitott;
        }
        static string Prefetch(string szoveg)
        {
            string atalakitott = "";
            for (int i = 0; i < szoveg.Length; i++)
            {
                if (char.IsLetterOrDigit(szoveg[i]))
                    atalakitott += char.ToLower(szoveg[i]);
            }
            return atalakitott;
        }        
        static bool Palindrom(string szoveg)
        {
            szoveg = Prefetch(szoveg);
            int j = 0;
            while (j < szoveg.Length / 2 &&
                szoveg[j] == szoveg[szoveg.Length - 1 - j])
            {
                j++;
            }
            return j >= szoveg.Length / 2;
        }

        static void PadLeftTeszt()
        {
            int[] arak = new int[] { 4990, 1490000, 990, 20, 15990 };
            int maxHossz = 10;
            for (int i = 0; i < arak.Length; i++)
            {
                string sor = arak[i].ToString();
                sor = sor.PadLeft(maxHossz, '.');
                sor += " Ft";
                Console.WriteLine(sor);
            }
        }

        static void Kozepre(string szoveg)
        {
            int maxHossz = Console.WindowWidth;
            string[] sorok = szoveg.Split('\n');
            for (int i = 0; i < sorok.Length; i++)
            {
                string sor = sorok[i];
                int behuzas = (maxHossz - sor.Length) / 2;
                sor = sor.PadLeft(behuzas + sor.Length);
                Console.WriteLine(sor);
            }
        }
    }
}
