﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_06_Kavezo
{
    class Program
    {
        static void Main(string[] args)
        {
            // sor: adott nap
            // oszlopok: aznapi italok

            Console.WriteLine(">> Italok bekérése");
            string[,] italokNaponta;
            italokNaponta = EladottItalokBekerese();

            Console.WriteLine("\n========================\n");
            Console.WriteLine(">> Feladatok: \n");

            /* Adott nap italai */
            Console.Write("Hanyadik nap eladásait szeretnéd listázni? ");
            int napIndex = int.Parse(Console.ReadLine()) - 1;
            string[] adottNapiItalok = AdottNapotKivalogat(italokNaponta, napIndex);
            Console.WriteLine($"\n{napIndex + 1}. napi italok:");
            for (int i = 0; i < adottNapiItalok.Length; i++)
                Console.WriteLine(adottNapiItalok[i]);

            /* Rendezés */
            NapiItallistaRendez(adottNapiItalok);

            /* IsmetlodesekKiszurese */
            string[] ismetlodesekNelkul = IsmetlodoItalokKiszurese(adottNapiItalok);
            Console.WriteLine("\n{0}. napi italok ismétlődések nélkül:", napIndex + 1);
            for (int i = 0; i < ismetlodesekNelkul.Length; i++)
                Console.WriteLine(ismetlodesekNelkul[i]);

            /* Italok */
            string[] osszesItal = Italok(italokNaponta);
            Console.WriteLine("\nEladott italok:");
            for (int i = 0; i < osszesItal.Length; i++)
                Console.WriteLine(osszesItal[i]);

            /* EladottMennyisegek */
            int[] mennyisegek = EladottMennyisegek(italokNaponta, osszesItal);
            Console.WriteLine("\nItalok + mennyiségek:");
            for (int i = 0; i < mennyisegek.Length; i++)
                Console.WriteLine($"{osszesItal[i]}: {mennyisegek[i]} darab");

            Console.ReadLine();
        }

        static int BekerNapokSzama() // itt lehetne egyéb hibakezelést megvalósítani, hibás inputokat szűrni stb.
        {
            int napokSzama;
            do
            {
                Console.Write("Hány nap eladásait szeretnéd rögzíteni? ");
                napokSzama = int.Parse(Console.ReadLine());
            } while (napokSzama < 2);
            return napokSzama;
        }
        static string[,] EladottItalokBekerese()
        {
            int napokSzama = BekerNapokSzama();  // sorok száma a 2D-s tömbben
            int MAX_ELADASOK_SZAMA_NAPONTA = 10; // oszlopok száma a 2D-s tömbben
            string[,] italokNaponta = new
                string[napokSzama, MAX_ELADASOK_SZAMA_NAPONTA];
            for (int i = 0; i < napokSzama; i++)
            {
                Console.WriteLine($"{i + 1}. napi italok (max 10 db, ha kevesebb, üss üres entert)");
                string ital;
                int napiDarabszam = 0;
                do
                {
                    ital = Console.ReadLine(); // a ReadLine() üres enter lenyomására üres stringgel "" tér vissza (NEM ugyanaz, mint a null érték!!!)
                    if (ital != "")
                    {
                        italokNaponta[i, napiDarabszam] = ital;
                        napiDarabszam++;
                    }
                } while (ital != "" && napiDarabszam < MAX_ELADASOK_SZAMA_NAPONTA);
            }
            return italokNaponta;
            // a kimeneti tömb feltöltetlen celláiban továbbra is null (érvénytelen memóriacím) szerepel
        }

        static int AdottNapiItalokSzama(string[,] italokNaponta, int napIndex)
        {
            int db = 0;
            while (db < italokNaponta.GetLength(1) && italokNaponta[napIndex, db] != null)
            {
                db++;
            }
            return db;
        }

        static string[] AdottNapotKivalogat(string[,] italokNaponta, int napIndex)
        {
            // először megvizsgáljuk, hány elemű tömbre lesz szükségünk
            // majd a bemeneti tömb megfelelő sorából átmásolunk annyi elemet a kimeneti tömbbe

            int db = AdottNapiItalokSzama(italokNaponta, napIndex);
            string[] adottNapiItalok = new string[db];
            for (int i = 0; i < db; i++)
            {
                adottNapiItalok[i] = italokNaponta[napIndex, i];
            }
            return adottNapiItalok;
        }

        static bool StringKisebbE(string elso, string masodik)
        {
            int j = 0;
            while (j < elso.Length && j < masodik.Length && elso[j] == masodik[j])
            {
                j++;
            }
            if (j >= elso.Length && j < masodik.Length)
                return true;
            if (j >= masodik.Length)
                return false;
            if (elso[j] < masodik[j])
                return true;
            return false;
        }

        static void Csere(ref string elso, ref string masodik) // cím szerint adjuk át a paramétereket, hogy tényleg megtörténjen a csere
        {
            string seged = elso;
            elso = masodik;
            masodik = seged;
        }

        static void NapiItallistaRendez(string[] italok)
        {
            // Minimumkiválasztásos rendezés
            for (int i = 0; i < italok.Length - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < italok.Length; j++)
                {
                    if (StringKisebbE(italok[j], italok[minIndex]))
                        minIndex = j;
                }
                Csere(ref italok[minIndex], ref italok[i]);
            }
        }

        static string[] IsmetlodoItalokKiszurese(string[] italok)
        {
            // helyben (bemeneti tömbben) szűr, azaz a tömb elejére pakolja a különbözőket

            int utolsoIndexe = 0; // az utolsó különböző elem helyét jelöli a tömbben (ha eddig az indexig nézzük a tömb elemeit, akkor nincs / nem lesz köztük ismétlődés)
            for (int i = 1; i < italok.Length; i++)
            {
                if (italok[i] != italok[utolsoIndexe])
                {
                    utolsoIndexe++;
                    italok[utolsoIndexe] = italok[i];
                }
            }
            if (utolsoIndexe + 1 == italok.Length) // ha nem volt ismétlődés
                return italok;
            else
            {
                string[] ujItalok = new string[utolsoIndexe + 1];
                for (int i = 0; i < ujItalok.Length; i++)
                {
                    ujItalok[i] = italok[i];
                }
                return ujItalok;
            }
        }

        static string[] KetListatOsszefuttat(string[] elsoLista, string[] masodikLista)
        {
            string[] ujLista = new string[elsoLista.Length + masodikLista.Length];
            int i = 0;
            int j = 0;
            int db = 0;
            while (i < elsoLista.Length && j < masodikLista.Length)
            {
                if (StringKisebbE(elsoLista[i], masodikLista[j]))
                {
                    ujLista[db] = elsoLista[i];
                    i++;
                }
                else if (StringKisebbE(masodikLista[j], elsoLista[i]))
                {
                    ujLista[db] = masodikLista[j];
                    j++;
                }
                else // ha a két string megegyezett egymással
                {
                    ujLista[db] = elsoLista[i];
                    i++;
                    j++;
                }
                db++;
            }
            while (i < elsoLista.Length)
            {
                ujLista[db] = elsoLista[i];
                i++;
                db++;
            }
            while (j < masodikLista.Length)
            {
                ujLista[db] = masodikLista[j];
                j++;
                db++;
            }
            string[] kimenet = new string[db];
            for (int k = 0; k < db; k++)
            {
                kimenet[k] = ujLista[k];
            }
            return kimenet;
        }

        static string[] Italok(string[,] italokNaponta)
        {
            // első napot kiválogatjuk, rendezzük, kiszedjük az ismétléseket
            // majd minden további napot kiválogatunk, rendezzük, kiszedjük az ismétléseket ÉS összefuttatjuk az eddigi listával

            string[] italLista = AdottNapotKivalogat(italokNaponta, 0);
            NapiItallistaRendez(italLista);
            italLista = IsmetlodoItalokKiszurese(italLista);
            for (int i = 1; i < italokNaponta.GetLength(0); i++)
            {
                string[] aktualisNapiItalok = AdottNapotKivalogat(italokNaponta, i);
                NapiItallistaRendez(aktualisNapiItalok);
                aktualisNapiItalok = IsmetlodoItalokKiszurese(aktualisNapiItalok);
                italLista = KetListatOsszefuttat(italLista, aktualisNapiItalok);
            }
            return italLista;
        }

        static int[] EladottMennyisegek(string[,] italokNaponta, string[] italok)
        {
            // két ciklus: végigmegyünk az összes eladott italon
            // ItalIndexe() metódus: minden italra meghatározzuk, hogy az az italok tömbben milyen indexen szerepel
            // majd a mennyisegek tömb ugyanezen indexen lévő elemét növeljük eggyel
            // így a mennyisegek tömb i-edik eleme azt adja meg, hogy az i-edik italból ténylegesen hány eladás történt

            int[] mennyisegek = new int[italok.Length];
            for (int napIndex = 0; napIndex < italokNaponta.GetLength(0); napIndex++)
            {
                int j = 0;
                while (j < italokNaponta.GetLength(1) && italokNaponta[napIndex, j] != null)
                {
                    string aktualisItal = italokNaponta[napIndex, j];
                    mennyisegek[ItalIndexe(italok, aktualisItal)]++;
                    j++;
                }
            }
            return mennyisegek;
        }

        static int ItalIndexe(string[] italok, string keresettItal)
        {
            // kiválasztás tétel
            int italIndex = 0;
            while (italok[italIndex] != keresettItal)
            {
                italIndex++;
            }
            return italIndex;
        }
    }
}
